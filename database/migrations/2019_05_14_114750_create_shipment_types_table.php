<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShipmentTypesTable extends Migration
{
    public function up(): void
    {
        Schema::create('shipment_types', function (Blueprint $table) {
            $table->string('uid')
                ->index()
                ->unique();
            $table->string('name');
            $table->string('en_name')
                ->nullable();
            $table->string('hazard_class')
                ->nullable();
            $table->string('hazard_code')
                ->nullable();
        });
    }
    
    
    public function down(): void
    {
        Schema::dropIfExists('shipment_types');
    }
}
