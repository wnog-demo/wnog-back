<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatusesTables extends Migration
{
    public function up(): void
    {
        Schema::create('statuses_green_light', function (Blueprint $table) {
            $table->unsignedInteger('id', true);
            
            $table->string('custom_clearance_uid')
                ->nullable();
            $table->foreign('custom_clearance_uid')
                ->references('uid')
                ->on('custom_clearances')
                ->onDelete('set null');
            
            $table->string('name');
            
            $table->string('comment')
                ->nullable();
            
            $table->string('en_comment')
                ->nullable();
            
            $table->timestamp('date_confirmed')
                ->nullable();
            
            $table->timestamp('date_status')
                ->nullable();
        });
        
        Schema::create('statuses_coming', function (Blueprint $table) {
            $table->unsignedInteger('id', true);
            
            $table->string('custom_clearance_uid')
                ->nullable();
            $table->foreign('custom_clearance_uid')
                ->references('uid')
                ->on('custom_clearances')
                ->onDelete('cascade')
                ->onDelete('set null');
            
            $table->string('name');
            
            $table->string('comment')
                ->nullable();
            
            $table->string('en_comment')
                ->nullable();
            
            $table->timestamp('date');
            
            $table->timestamp('date_status')
                ->nullable();
        });
        
        Schema::create('statuses_declaration', function (Blueprint $table) {
            $table->unsignedInteger('id', true);
            
            $table->string('declaration_uid')
                ->nullable();
            $table->foreign('declaration_uid')
                ->references('uid')
                ->on('declarations')
                ->onDelete('set null');
            
            $table->string('name');
            
            $table->string('comment')
                ->nullable();
            
            $table->string('en_comment')
                ->nullable();
            
            $table->timestamp('date_status')
                ->nullable();
        });
        
        Schema::create('statuses_transportation', function (Blueprint $table) {
            $table->unsignedInteger('id', true);
            
            $table->string('custom_clearance_uid')
                ->nullable();
            $table->foreign('custom_clearance_uid')
                ->references('uid')
                ->on('custom_clearances')
                ->onDelete('set null');
            
            $table->string('name');
            
            $table->string('comment')
                ->nullable();
            
            $table->string('en_comment')
                ->nullable();
            
            $table->timestamp('date_status')
                ->nullable();
        });
    }
    
    
    public function down(): void
    {
        Schema::dropIfExists('statuses_green_light');
        Schema::dropIfExists('statuses_coming');
        Schema::dropIfExists('statuses_declaration');
        Schema::dropIfExists('statuses_transportation');
    }
}
