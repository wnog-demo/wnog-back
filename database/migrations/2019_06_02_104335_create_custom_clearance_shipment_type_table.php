<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomClearanceShipmentTypeTable extends Migration
{
    public function up(): void
    {
        Schema::create('custom_clearance_shipment_type', function (Blueprint $table) {
            $table->string('custom_clearance_uid');
            $table->foreign('custom_clearance_uid')
                ->references('uid')
                ->on('custom_clearances')
                ->onDelete('cascade');
            
            $table->string('shipment_type_uid');
            $table->foreign('shipment_type_uid')
                ->references('uid')
                ->on('shipment_types')
                ->onDelete('cascade');
            
            $table->string('quantity')
                ->nullable();
            
            $table->string('weight')
                ->nullable();
        });
    }
    
    
    public function down(): void
    {
        Schema::dropIfExists('custom_clearance_shipment_type');
    }
}
