<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationsTables extends Migration
{
    public function up(): void
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->unsignedInteger('id', true);
            $table->unsignedInteger('sort')
                ->nullable()
                ->default(100);
            $table->string('name');
            $table->string('en_name')
                ->nullable();
            $table->string('slug');
            $table->boolean('is_active')
                ->default(true);
        });
        
        DB::table('notifications')->insert([
            [
                'name'    => 'О получении запроса на Green Light',
                'en_name' => 'About receiving a request for Green Light',
                'slug'    => 'greenlight-request-received',
                'sort'    => 1,
            ],
            [
                'name'    => 'О подтверждении Green Light',
                'en_name' => 'About Green Light Verification',
                'slug'    => 'greenlight-confirmed',
                'sort'    => 2,
            ],
            [
                'name'    => 'Когда груз прибывает на таможню',
                'en_name' => 'When cargo arrives at customs',
                'slug'    => 'coming-ata',
                'sort'    => 3,
            ],
            [
                'name'    => 'Когда ДТ зарегистрирована',
                'en_name' => 'When DT is registered',
                'slug'    => 'declaration-registered',
                'sort'    => 4,
            ],
            [
                'name'    => 'О назначении таможенного досмотра',
                'en_name' => 'On the appointment of customs clearance',
                'slug'    => 'declaration-appointment',
                'sort'    => 5,
            ],
            [
                'name'    => 'О завершении таможенного досмотра',
                'en_name' => 'On completion of customs clearance',
                'slug'    => 'declaration-completion',
                'sort'    => 6,
            ],
            [
                'name'    => 'Об условном выпуске декларации',
                'en_name' => 'When shipment is shipped',
                'slug'    => 'declaration-shipped',
                'sort'    => 7,
            ],
            [
                'name'    => 'Когда ДТ выпущена',
                'en_name' => 'When DT is released',
                'slug'    => 'declaration-released',
                'sort'    => 8,
            ],
            [
                'name'    => 'Когда груз отправлен',
                'en_name' => 'When shipment is shipped',
                'slug'    => 'transportation-cargo-shipped',
                'sort'    => 9,
            ],
            [
                'name'    => 'Об отказе в выпуске декларации',
                'en_name' => 'When refusal to issue a declaration',
                'slug'    => 'declaration-refusal',
                'sort'    => 10,
            ],
        ]);
        
        Schema::create('notification_user', function (Blueprint $table) {
            $table->integer('notification_id')
                ->unsigned();
            $table->foreign('notification_id')
                ->references('id')
                ->on('notifications')
                ->onDelete('cascade');
            $table->string('user_uid');
            $table->foreign('user_uid')
                ->references('uid')
                ->on('users')
                ->onDelete('cascade');
            $table->boolean('value')
                ->default(false);
        });
    }
    
    
    public function down(): void
    {
        Schema::dropIfExists('notification_user');
        Schema::dropIfExists('notifications');
    }
}
