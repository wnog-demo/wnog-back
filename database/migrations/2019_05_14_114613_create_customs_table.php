<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomsTable extends Migration
{
    public function up(): void
    {
        Schema::create('customs', function (Blueprint $table) {
            $table->string('uid')
                ->index()
                ->unique();

            $table->string('name');

            $table->string('fullname')
                ->nullable();

            $table->string('en_fullname')
                ->nullable();

            $table->string('city')
                ->nullable();
                
            $table->string('en_city')
                ->nullable();
        });
    }
    
    
    public function down(): void
    {
        Schema::dropIfExists('customs');
    }
}
