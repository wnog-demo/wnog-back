<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatusMailsTable extends Migration
{
    public function up(): void
    {
        Schema::create('status_mails', function (Blueprint $table) {
            $table->unsignedInteger('id', true);
            
            $table->string('custom_clearance_uid');
            
            $table->unsignedInteger('status_id');
            
            $table->string('type');
            
            $table->timestamps();
        });
    }
    
    
    public function down(): void
    {
        Schema::dropIfExists('status_mails');
    }
}
