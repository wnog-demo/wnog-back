<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeclarationsTable extends Migration
{
    public function up(): void
    {
        Schema::create('declarations', function (Blueprint $table) {
            $table->unsignedInteger('id', true);
            
            $table->string('uid')
                ->unique();
            
            $table->string('custom_clearance_uid')
                ->nullable();
            $table->foreign('custom_clearance_uid')
                ->references('uid')
                ->on('custom_clearances')
                ->onDelete('set null');
            
            $table->string('number')
                ->nullable();
            
            $table->timestamp('date')
                ->nullable();
        });
    }
    
    
    public function down(): void
    {
        Schema::dropIfExists('declarations');
    }
}
