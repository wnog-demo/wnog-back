<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVesselsTable extends Migration
{
    public function up(): void
    {
        Schema::create('vessels', function (Blueprint $table) {
            $table->string('uid')
                ->index()
                ->unique();
            $table->string('name');
        });
    }
    
    
    public function down(): void
    {
        Schema::dropIfExists('vessels');
    }
}
