<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomClearanceLogsTable extends Migration
{
    public function up(): void
    {
        Schema::create('custom_clearance_logs', function (Blueprint $table) {
            $table->unsignedInteger('id', true);
            
            $table->string('user_uid');
            $table->foreign('user_uid')
                ->references('uid')
                ->on('users');
            
            $table->json('action');
            
            $table->date('date')
                ->useCurrent();
            
            $table->timestamp('created_at')
                ->useCurrent();
        });
    }
    
    
    public function down(): void
    {
        Schema::dropIfExists('custom_clearance_logs');
    }
}
