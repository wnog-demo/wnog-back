<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomClearancesTable extends Migration
{
    public function up(): void
    {
        Schema::create('custom_clearances', function (Blueprint $table) {
            $table->string('uid')
                ->index()
                ->unique();
            
            $table->enum('type', ['import', 'export']);
            
            $table->timestamp('date');
            
            $table->string('reference')
                ->nullable();
            
            $table->string('user_uid')
                ->nullable();
            $table->foreign('user_uid')
                ->references('uid')
                ->on('users')
                ->onDelete('set null');
            
            $table->string('contact_uid')
                ->nullable();
            $table->foreign('contact_uid')
                ->references('uid')
                ->on('contacts')
                ->onDelete('set null');
            
            $table->string('customer_uid')
                ->nullable();
            $table->foreign('customer_uid')
                ->references('uid')
                ->on('clients')
                ->onDelete('set null');
            
            $table->string('custom_uid')
                ->nullable();
            $table->foreign('custom_uid')
                ->references('uid')
                ->on('customs')
                ->onDelete('set null');
            
            $table->string('vessel_uid')
                ->nullable();
            $table->foreign('vessel_uid')
                ->references('uid')
                ->on('vessels')
                ->onDelete('set null');
            
            $table->string('shipper_uid')
                ->nullable();
            $table->foreign('shipper_uid')
                ->references('uid')
                ->on('shippers')
                ->onDelete('set null');
            
            $table->string('consignee_uid')
                ->nullable();
            $table->foreign('consignee_uid')
                ->references('uid')
                ->on('shippers')
                ->onDelete('set null');
            
            $table->string('product_uid')
                ->nullable();
            $table->foreign('product_uid')
                ->references('uid')
                ->on('products')
                ->onDelete('set null');
            
            $table->string('port_of_discharge_uid')
                ->nullable();
            $table->foreign('port_of_discharge_uid')
                ->references('uid')
                ->on('places')
                ->onDelete('set null');
            
            $table->string('port_of_loading_uid')
                ->nullable();
            $table->foreign('port_of_loading_uid')
                ->references('uid')
                ->on('places')
                ->onDelete('set null');
            
            $table->string('place_of_receipt_uid')
                ->nullable();
            $table->foreign('place_of_receipt_uid')
                ->references('uid')
                ->on('places')
                ->onDelete('set null');
            
            $table->string('place_of_delivery_uid')
                ->nullable();
            $table->foreign('place_of_delivery_uid')
                ->references('uid')
                ->on('places')
                ->onDelete('set null');
            
            // Коносамент
            $table->string('bol')
                ->nullable();
            
            $table->string('awb')
                ->nullable();
            
            $table->string('rwb')
                ->nullable();
            
            $table->string('cmr')
                ->nullable();
            
            $table->boolean('is_archive')
                ->default(false);
            
            $table->timestamps();
        });
    }
    
    
    public function down(): void
    {
        Schema::dropIfExists('custom_clearances');
    }
}
