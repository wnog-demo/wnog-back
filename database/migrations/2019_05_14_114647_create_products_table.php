<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    public function up(): void
    {
        Schema::create('products', function (Blueprint $table) {
            $table->string('uid')
                ->index()
                ->unique();
            
            $table->string('name');
            
            $table->string('en_name')
                ->nullable();
        });
    }
    
    
    public function down(): void
    {
        Schema::dropIfExists('products');
    }
}
