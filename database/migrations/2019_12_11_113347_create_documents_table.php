<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentsTable extends Migration
{
    public function up(): void
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->string('uid')
                ->index()
                ->unique();
            $table->string('custom_clearance_uid')
                ->nullable();
            $table->foreign('custom_clearance_uid')
                ->references('uid')
                ->on('custom_clearances')
                ->onDelete('set null');
            $table->string('path');
            $table->string('name');
            $table->string('en_name')
                ->nullable();
            $table->string('category_name');
            $table->string('en_category_name')
                ->nullable();
            $table->timestamp('date')
                ->nullable();
            $table->timestamps();
        });
    }
    
    
    public function down(): void
    {
        Schema::dropIfExists('documents');
    }
}
