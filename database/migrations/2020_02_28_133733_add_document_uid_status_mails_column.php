<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddDocumentUidStatusMailsColumn extends Migration
{
    public function up(): void
    {
        Schema::table('status_mails', function (Blueprint $table) {
            $table->string('document_uid')
                ->nullable();
        });
        
        DB::table('notifications')
            ->insert([
                'name'    => 'О загрузке новых документов',
                'en_name' => 'About uploading new documents',
                'slug'    => 'document-create',
                'sort'    => 11,
            ]);
    }
}
