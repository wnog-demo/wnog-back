<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveIsArchiveCustomClearanceColumn extends Migration
{
    public function up()
    {
        Schema::table('custom_clearances', function (Blueprint $table) {
            $table->dropColumn('is_archive');
        });
    }
}
