<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShippersTable extends Migration
{
    public function up(): void
    {
        Schema::create('shippers', function (Blueprint $table) {
            $table->string('uid')
                ->index()
                ->unique();
            $table->string('name');
            $table->string('en_name')
                ->nullable();
            $table->string('address')
                ->nullable();
        });
    }
    
    
    public function down(): void
    {
        Schema::dropIfExists('shippers');
    }
}
