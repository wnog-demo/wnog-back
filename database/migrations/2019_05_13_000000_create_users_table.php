<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    public function up(): void
    {
        Schema::create('user_roles', function (Blueprint $table) {
            $table->unsignedInteger('id', true);
            $table->string('name');
            $table->string('slug');
        });
        
        Schema::create('users', function (Blueprint $table) {
            $table->string('uid')
                ->index()
                ->unique();
            
            $table->integer('role_id')
                ->unsigned()
                ->nullable();
            $table->foreign('role_id')
                ->references('id')
                ->on('user_roles')
                ->onDelete('set null');
            
            $table->string('client_uid')
                ->nullable();
            $table->foreign('client_uid')
                ->references('uid')
                ->on('clients')
                ->onDelete('set null');
            
            $table->boolean('is_active')
                ->default(false);
            
            $table->string('name');
            
            $table->string('en_name')
                ->nullable();
            
            $table->string('email')
                ->unique();
            
            $table->enum('gender', ['male', 'female'])
                ->nullable();
            
            $table->string('password')
                ->nullable();
            
            $table->string('password_hash')
                ->nullable();
            
            $table->timestamps();
        });
        
        DB::table('user_roles')
            ->insert([
                ['name' => 'Менеджер', 'slug' => 'manager'],
                ['name' => 'Координатор', 'slug' => 'coordinator'],
            ]);
    }
    
    
    public function down(): void
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('user_roles');
    }
}
