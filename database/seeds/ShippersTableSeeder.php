<?php

use App\Models\Shipper;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Database\Seeder;

class ShippersTableSeeder extends Seeder
{
    public function run(): void
    {
        try {
            $rows = json_decode(File::get('database/schema/shippers.json'), true);
            
            foreach ($rows as $i => $row) {
                Shipper::create($row);
            }
        } catch (FileNotFoundException $exception) {
            $this->command->error($exception->getMessage());
        }
    }
}
