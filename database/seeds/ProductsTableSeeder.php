<?php

use App\Models\Product;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    public function run(): void
    {
        try {
            $rows = json_decode(File::get('database/schema/products.json'), true);
            
            foreach ($rows as $i => $row) {
                Product::create($row);
            }
        } catch (FileNotFoundException $exception) {
            $this->command->error($exception->getMessage());
        }
    }
}
