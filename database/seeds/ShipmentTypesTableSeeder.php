<?php

use App\Models\ShipmentType;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Database\Seeder;

class ShipmentTypesTableSeeder extends Seeder
{
    public function run(): void
    {
        try {
            $rows = json_decode(File::get('database/schema/shipment_types.json'), true);
            
            foreach ($rows as $i => $row) {
                ShipmentType::create($row);
            }
        } catch (FileNotFoundException $exception) {
            $this->command->error($exception->getMessage());
        }
    }
}
