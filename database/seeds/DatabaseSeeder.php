<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run(): void
    {
        $this->call([
            TablesSeeder::class
            // ClientsTableSeeder::class,
            // UsersTableSeeder::class,
            // VesselsTableSeeder::class,
            // ShippersTableSeeder::class,
            // CustomsTableSeeder::class,
            // ProductsTableSeeder::class,
            // PlacesTableSeeder::class,
            // ShipmentTypesTableSeeder::class,
            // ContactsTableSeeder::class,
            // CustomClearancesTableSeeder::class,
            // DeclarationsTableSeeder::class,
            // StatusesGreenLightTableSeeder::class,
            // StatusesComingTableSeeder::class,
            // StatusesDeclarationTableSeeder::class,
            // StatusesTransportationTableSeeder::class,
            // CustomClearanceShipmentTypeTableSeeder::class,
        ]);
    }
}
