<?php

use App\Models\Client;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Database\Seeder;

class CustomClearanceShipmentTypeTableSeeder extends Seeder
{
    public function run(): void
    {
        try {
            $rows = json_decode(File::get('database/schema/custom_clearance_shipment_type.json'), true);
            
            foreach ($rows as $i => $row) {
                DB::table('custom_clearance_shipment_type')->insert($row);
            }
        } catch (FileNotFoundException $exception) {
            $this->command->error($exception->getMessage());
        }
    }
}
