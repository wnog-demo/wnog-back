<?php

use App\Models\Stage\Transportation;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Database\Seeder;

class StatusesTransportationTableSeeder extends Seeder
{
    public function run(): void
    {
        try {
            $rows = json_decode(File::get('database/schema/statuses_transportation.json'), true);
            
            foreach ($rows as $i => $row) {
                Transportation::create($row);
            }
        } catch (FileNotFoundException $exception) {
            $this->command->error($exception->getMessage());
        }
    }
}
