<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class TablesSeeder extends Seeder
{
    public function run(): void
    {
        try {
            $dump = File::get('database/dumps/mariadb.sql');
            
            if (! DB::unprepared($dump)) {
                throw new Exception('the database was not imported');
            }
            static::updateUsers();
        } catch (Exception $exception) {
            $this->command->error($exception->getMessage());
        }
    }
    
    
    protected static function updateUsers()
    {
        User::where('is_active', true)
            ->get()
            ->each(function (User $user) {
                $user->update([
                    'password' => Hash::make('password'),
                    'email'    => md5($user->email) . '@dev.nimax.ru',
                ]);
            });
    }
}
