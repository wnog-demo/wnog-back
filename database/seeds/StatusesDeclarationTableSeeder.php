<?php

use App\Models\Stage\Declaration;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Database\Seeder;

class StatusesDeclarationTableSeeder extends Seeder
{
    public function run(): void
    {
        try {
            $rows = json_decode(File::get('database/schema/statuses_declaration.json'), true);
            
            foreach ($rows as $i => $row) {
                Declaration::create($row);
            }
        } catch (FileNotFoundException $exception) {
            $this->command->error($exception->getMessage());
        }
    }
}
