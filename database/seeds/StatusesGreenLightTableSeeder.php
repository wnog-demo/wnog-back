<?php

use App\Models\Stage\GreenLight;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Database\Seeder;

class StatusesGreenLightTableSeeder extends Seeder
{
    public function run(): void
    {
        try {
            $rows = json_decode(File::get('database/schema/statuses_green_light.json'), true);
            
            foreach ($rows as $i => $row) {
                GreenLight::create($row);
            }
        } catch (FileNotFoundException $exception) {
            $this->command->error($exception->getMessage());
        }
    }
}
