<?php

use App\Models\Custom;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Database\Seeder;

class CustomsTableSeeder extends Seeder
{
    public function run(): void
    {
        try {
            $rows = json_decode(File::get('database/schema/customs.json'), true);
            
            foreach ($rows as $i => $row) {
                Custom::create($row);
            }
        } catch (FileNotFoundException $exception) {
            $this->command->error($exception->getMessage());
        }
    }
}
