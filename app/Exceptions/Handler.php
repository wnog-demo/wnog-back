<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Response;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    protected $dontReport = [];
    
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];
    
    
    public function report(Exception $exception): void
    {
        parent::report($exception);
    }
    
    
    public function render($request, Exception $exception)
    {
        if ($exception instanceof NotFoundHttpException) {
            return self::notFound();
        }
        
        return parent::render($request, $exception);
    }
    
    
    protected static function notFound()
    {
        return response()->json([
            'status'  => 'error',
            'message' => 'Method not found.',
        ], 404);
    }
}
