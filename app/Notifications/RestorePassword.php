<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class RestorePassword extends Notification
{
    use Queueable;
    
    
    public function __construct()
    {
    }
    
    
    public function via($notifiable): array
    {
        return ['mail'];
    }
    
    
    public function toMail($notifiable): MailMessage
    {
        return (new MailMessage)->bcc(config('mail.cc'))
                                ->subject('Восстановление пароля в аккаунте eWNOG!')
                                ->view('mail.restorepassword', ['user' => $notifiable]);
    }
}
