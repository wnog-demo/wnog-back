<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NewUser extends Notification
{
    use Queueable;
    
    
    public function __construct()
    {
    }
    
    
    public function via($notifiable): array
    {
        return ['mail'];
    }
    
    
    public function toMail($notifiable): MailMessage
    {
        return (new MailMessage)
            ->bcc(config('mail.cc'))
            ->subject('Ваша учетная запись в аккаунте eWNOG создана!')
            ->view('mail.setpassword', ['user' => $notifiable]);
    }
}
