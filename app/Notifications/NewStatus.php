<?php

namespace App\Notifications;

use App\Contracts\StageInterface;
use App\Models\CustomClearance;
use App\Models\Document;
use App\Models\Notification;
use App\Models\Stage\Declaration;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification as NotificationIlluminate;
use Illuminate\Support\Arr;

class NewStatus extends NotificationIlluminate
{
    use Queueable;
    
    protected $subject;
    
    protected $data;
    
    protected static $singleData = [
        'greenlight-request-received'  => [
            'subject' => 'Запрос на Green Light получен',
            'html'    => 'Благодарим за Ваш запрос. Документы по грузу #reference# #product# находятся на рассмотрении. С комментариями вернемся дополнительно.<br><br>Координатор #contact#.',
        ],
        'greenlight-confirmed'         => [
            'subject' => 'Green Light подтвержден',
            'html'    => 'GL для груза #reference# #product# подтверждаем. Комментарии направлены отдельным письмом. Ожидаем pre-alert.<br><br>Координатор #contact#.',
        ],
        'coming-ata'                   => [
            'subject' => 'Груз прибыл на таможню',
            'html'    => 'Груз #reference# #product# прибыл в пункт таможенного оформления. Таможня — #custom#. ДТ готовится к подаче.<br><br>Координатор #contact#.',
        ],
        'declaration-registered'       => [
            'subject' => 'Декларация на товары зарегистрирована',
            'html'    => 'Зарегистрирована ДТ №#number#. В настоящий момент осуществляется проверка заявленных документов и сведений.<br><br>Координатор #contact#.',
        ],
        // assigned-inspection
        'declaration-appointment'      => [
            'subject' => 'Назначен досмотр',
            'html'    => 'Для груза #reference# #product# назначен досмотр.<br><br>Координатор #contact#.',
        ],
        // inspection-completed
        'declaration-completion'       => [
            'subject' => 'Досмотр завершен',
            'html'    => 'Досмотр для груза #reference# #product# завершен.<br><br>Координатор #contact#.',
        ],
        // conditional-release
        'declaration-shipped'          => [
            'subject' => 'Условный выпуск',
            'html'    => 'Для груза #reference# #product# оформлен условный выпуск товаров. С комментариями вернемся дополнительно отдельным письмом.<br><br>Координатор #contact#.',
        ],
        'declaration-released'         => [
            'subject' => 'Декларация на товары выпущена',
            'html'    => 'Таможенное оформление по ДТ №#number# для груза #reference# #product# завершено.<br><br>Координатор #contact#.',
        ],
        // refusal-to-issue
        'declaration-refusal'          => [
            'subject' => 'Декларация на товары отказ в выпуске',
            'html'    => 'По ДТ №#number# для груза #reference# #product# было принято решение об отказе в выпуске товаров.<br><br>Координатор #contact#.',
        ],
        'transportation-cargo-shipped' => [
            'subject' => 'Груз отправлен',
            'html'    => 'Груз #reference# #product# отправлен с СВХ. Для получения подробной информации о статусе транспортировки просим связаться с #contact#.',
        ],
        'document-create'              => [
            'subject' => 'Загружен новый документ',
            'html'    => 'По таможенному оформлению для груза #reference# был загружен новый документ.<br><br>Координатор #contact#.',
        ],
    ];
    
    protected static $multiData = [
        'greenlight-request-received'  => [
            'subject' => 'Запрос на Green Light получен',
            'html'    => 'Документы находятся на рассмотрении. С комментариями вернемся дополнительно.',
        ],
        'greenlight-confirmed'         => [
            'subject' => 'Green Light подтвержден',
            'html'    => 'GL для груза подтверждаем. Комментарии направлены отдельным письмом. Ожидаем pre-alert.',
        ],
        'coming-ata'                   => [
            'subject' => 'Груз прибыл на таможню',
            'html'    => 'Груз прибыл в пункт таможенного оформления. Таможня — #custom#. ДТ готовится к подаче.',
        ],
        'declaration-registered'       => [
            'subject' => 'Декларация на товары зарегистрирована',
            'html'    => 'Зарегистрирована ДТ №#number#. В настоящий момент осуществляется проверка заявленных документов и сведений.',
        ],
        'declaration-appointment'      => [
            'subject' => 'Назначен досмотр',
            'html'    => 'Назначен досмотр.',
        ],
        'declaration-completion'       => [
            'subject' => 'Досмотр завершен',
            'html'    => 'Досмотр завершен.',
        ],
        'declaration-shipped'          => [
            'subject' => 'Условный выпуск',
            'html'    => 'Оформлен условный выпуск товаров. С комментариями вернемся дополнительно отдельным письмом.',
        ],
        'declaration-released'         => [
            'subject' => 'Декларация на товары выпущена',
            'html'    => 'Таможенное оформление по ДТ №#number# завершено.',
        ],
        'declaration-refusal'          => [
            'subject' => 'Декларация на товары отказ в выпуске',
            'html'    => 'По ДТ №#number# было принято решение об отказе в выпуске товаров.',
        ],
        'transportation-cargo-shipped' => [
            'subject' => 'Груз отправлен',
            'html'    => 'Груз отправлен с СВХ. Для получения подробной информации о статусе транспортировки просим связаться с #contact#.',
        ],
        'document-create'              => [
            'subject' => 'Загружены новые документы',
            'html'    => 'По таможенному оформлению для груза #reference# были загружены новые документы.',
        ],
    ];
    
    
    public function __construct(string $subject, array $data)
    {
        $this->subject = $subject;
        $this->data = $data;
    }
    
    
    public function via($notifiable): array
    {
        return ['mail'];
    }
    
    
    public function toMail($notifiable): MailMessage
    {
        return (new MailMessage)->subject($this->subject)
            ->bcc(config('mail.cc'))
            ->view('mail.stage', ['data' => $this->data]);
    }
    
    
    protected static function sort(array $keys, array $items)
    {
        $data = [];
        
        foreach ($keys as $key) {
            foreach ($items as $item) {
                if ($key == $item['slug']) {
                    $data[] = $item;
                }
            }
        }
        
        return $data;
    }
    
    
    public static function data($items, $users)
    {
        $keys = array_keys(self::$singleData);
        $items = self::sort($keys, $items);
        
        if (count($items) > 1) {
            return self::renderMulti($items, $users);
        }
        
        return self::render(Arr::first($items), $users);
    }
    
    
    protected static function renderMulti($items, $users)
    {
        $first = Arr::first($items);
        $stage = $first['stage'];
        
        /** @var CustomClearance $clearance */
        if (! is_null($stage->customClearance)) {
            /** @var StageInterface $stage */
            $clearance = $stage->customClearance;
        } elseif (! is_null($stage->custom_clearance)) {
            /** @var Document $stage */
            $clearance = $stage->custom_clearance;
        } else {
            /** @var Declaration $stage */
            $clearance = $stage->declaration->custom_clearance;
        }
        
        $fields = [
            '#reference#' => ! is_null($clearance->reference) ? $clearance->reference : '',
            '#custom#'    => ! is_null($clearance->custom) ? $clearance->custom->name : '',
            '#contact#'   => ! is_null($clearance->contact) ? $clearance->contact->name : '',
        ];
        
        $results = [];
        
        /** @var User $user */
        foreach ($users as $user) {
            $subject = sprintf('%s статус: ', $clearance->reference);
            $html = self::trim(sprintf('Благодарим за Ваш запрос по грузу %s. Сообщаем Вам статус:', $clearance->reference)) . self::br();
            $subjects = $ul = [];
            $stages = [];
            
            foreach ($items as $item) {
                if (! empty($user->notifications) && ! in_array($item['slug'], $user->notifications->map(function (Notification $notification) {
                        return $notification->slug;
                    })
                        ->toArray())) {
                    continue;
                }
                
                $f = $fields;
                
                /** @var StageInterface $stage */
                $stage = $item['stage'];
                
                if (! is_null($stage->declaration)) {
                    $f['#number#'] = $stage->declaration->number;
                }
                
                if ($item['slug'] == 'coming-ata') {
                    $f['#custom#'] = ! is_null($clearance->custom) ? $clearance->custom->fullname : '';
                }
                
                $values = self::$multiData[$item['slug']];
                
                if (! in_array($values['subject'], $subjects)) {
                    $subjects[] = $values['subject'];
                }
                
                if (! in_array($item['slug'], $stages)) {
                    if (! is_null($stage->date_status)) {
                        $html .= self::trim($stage->date_status->format('d.m.Y') . ' – ' . str_replace(array_keys($f), $f, $values['html']));
                    } else {
                        $html .= self::trim(str_replace(array_keys($f), $f, $values['html']));
                    }
                    
                    $html .= self::br();
                }
                
                $stages[] = $item['slug'];
            }
            
            $multi = true;
            
            if (empty($stages)) {
                continue;
            } elseif (count($stages) == 1) {
                $multi = false;
                $values = self::$singleData[Arr::first($stages)];
                $subject .= $values['subject'];
                $f = array_merge($f, [
                    '#reference#' => $clearance->reference,
                    '#product#'   => ! is_null($clearance->product) ? '(' . $clearance->product->name . ')' : '',
                ]);
                $html = self::trim(str_replace(array_keys($f), $f, $values['html']));
            } else {
                $subject .= implode(', ', $subjects);
            }
            
            if (($contact = $clearance->contact) && $multi) {
                $html .= self::br() . self::trim(sprintf('Координатор ВНОГ — %s', $contact->name));
            }
            
            $results[$user->uid] = [
                $subject,
                [
                    'url'  => sprintf('%s/clearance/%s', config('app.url-front'), $clearance->uid),
                    'html' => $html,
                ],
            ];
        }
        
        return $results;
    }
    
    
    protected static function render($item, $users)
    {
        $results = [];
        
        /** @var User $user */
        foreach ($users as $user) {
            if (! empty($user->notifications) && ! in_array($item['slug'], $user->notifications->map(function (Notification $notification) {
                    return $notification->slug;
                })
                    ->toArray())) {
                continue;
            }
            
            $stage = $item['stage'];
            
            if (! is_null($stage->customClearance)) {
                /** @var StageInterface $stage */
                $clearance = $stage->customClearance;
            } elseif (! is_null($stage->custom_clearance)) {
                /** @var Document $stage */
                $clearance = $stage->custom_clearance;
            } else {
                /** @var Declaration $stage */
                $clearance = $stage->declaration->custom_clearance;
            }
            
            $fields = [
                '#custom#'    => ! is_null($clearance->custom) ? $clearance->custom->name : '',
                '#contact#'   => ! is_null($clearance->contact) ? $clearance->contact->name : '',
                '#reference#' => $clearance->reference,
                '#product#'   => ! is_null($clearance->product) ? '(' . $clearance->product->name . ')' : '',
                '#number#'    => ! is_null($stage->declaration) ? $stage->declaration->number : '',
            ];
            $values = self::$singleData[$item['slug']];
            $subject = sprintf('%s статус: %s', $clearance->reference, $values['subject']);
            $html = self::trim(str_replace(array_keys($fields), $fields, $values['html']));
            $results[$user->uid] = [
                $subject,
                [
                    'url'  => sprintf('%s/clearance/%s', config('app.url-front'), $clearance->uid),
                    'html' => $html,
                ],
            ];
        }
        
        return $results;
    }
    
    
    protected static function trim(string $string)
    {
        $before = '<p style="margin: 0; padding: 0; font-size: 16px; font-weight: bold; line-height: 26px; max-width: 550px;"><font face="sans-serif" style="margin: 0; padding: 0; color: #252841;">';
        $after = '</font></p>';
        
        return $before . trim($string) . $after;
    }
    
    
    protected static function br()
    {
        return '<table style="margin: 0; padding: 0; border-spacing: 0; border: none;"><tr style="margin: 0; padding: 0;"><td height="25" style="margin: 0; padding: 0;"></td></tr></table>';
    }
}
