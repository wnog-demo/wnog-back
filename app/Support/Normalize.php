<?php

namespace App\Support;

class Normalize
{
    public static function int(string $string): ?int
    {
        return preg_replace('/[^0-9]/', '', $string) ?: null;
    }
    
    
    public static function phone(string $phone, bool $with7 = false): ?string
    {
        $phone = self::int($phone);
        $phone = substr($phone, -10);
        
        if (strlen($phone) !== 10) {
            return null;
        }
        
        if ($with7) {
            $phone = '7' . $phone;
        }
        
        return $phone;
    }
}
