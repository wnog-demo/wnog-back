<?php

namespace App\Support;

use Eloquent;
use Illuminate\Auth\Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User extends Eloquent implements JWTSubject, AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;
    
    
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    
    
    public function getJWTCustomClaims(): array
    {
        return [];
    }
}
