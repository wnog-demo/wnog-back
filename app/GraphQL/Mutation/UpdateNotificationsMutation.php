<?php

namespace App\GraphQL\Mutation;

use DB;
use Auth;
use App\Models\User;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;

class UpdateNotificationsMutation extends Mutation
{
    protected $attributes = [
        'name'        => 'UpdateNotifications',
        'description' => 'Обновление полей для уведомлений',
    ];
    
    
    public function authorize(array $args)
    {
        return User::check();
    }
    
    
    public function type()
    {
        return Type::boolean();
    }
    
    
    public function args()
    {
        return [
            'values' => [
                'name' => 'values',
                'type' => Type::nonNull(Type::string()),
            ],
        ];
    }
    
    
    public function resolve($root, $args)
    {
        $uid = Auth::id();
        $values = json_decode($args['values'], true);
        
        if (empty($values) || empty($uid)) {
            return false;
        }
        
        foreach ($values as $notificationId => $value) {
            DB::table('notification_user')
                ->where('notification_id', $notificationId)
                ->where('user_uid', $uid)
                ->update(['value' => $value]);
        }
        
        return true;
    }
}
