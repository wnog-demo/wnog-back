<?php

namespace App\GraphQL\Mutation;

use App\Models\Document;
use App\Models\User;
use Exception;
use GraphQL\Type\Definition\Type;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Rebing\GraphQL\Support\Mutation;
use Storage;
use ZipArchive;

class DownloadDocumentsMutation extends Mutation
{
    protected $attributes = [
        'name'        => 'DownloadDocuments',
        'description' => 'Скачивание документов',
    ];
    
    
    public function authorize(array $args)
    {
        return User::check();
    }
    
    
    public function type()
    {
        return Type::string();
    }
    
    
    public function args()
    {
        return [
            'uids' => [
                'name' => 'uids',
                'type' => Type::listOf(Type::string()),
            ],
        ];
    }
    
    
    public function resolve($root, $args)
    {
        $documents = Document::whereIn('uid', $args['uids'])
            ->get();
        
        if ($documents->isEmpty()) {
            throw new Exception('documents not found');
        }
        
        if (! Storage::exists('public/downloads')) {
            Storage::makeDirectory('public/downloads');
        }
        
        if ($documents->count() > 1) {
            return static::archive($documents);
        } else {
            return static::single($documents);
        }
    }
    
    
    /**
     * @param Document|Collection $documents
     *
     * @return string
     */
    protected static function single($documents)
    {
        $document = $documents->first();
        $path = sprintf('public%s', $document->path);
        $to = sprintf('public/downloads/%s.%s', $document->name, pathinfo($path)['extension']);
        
        if (Storage::exists($to)) {
            return config('app.url') . str_replace('public/', '/', $to);
        }
        
        Storage::copy($path, $to);
        
        return config('app.url') . str_replace('public/', '/', $to);
    }
    
    
    /**
     * @param Document|Collection $documents
     *
     * @return string
     */
    protected static function archive($documents)
    {
        $customClearance = $documents->first()->custom_clearance;
        $path = sprintf('public/downloads/%s.zip', Str::upper(Str::slug($customClearance->reference)));
        $zipfilename = Storage::path($path);
        
        if (Storage::exists($path)) {
            Storage::delete($path);
        }
        
        $zip = new ZipArchive();
        $zip->open($zipfilename, ZipArchive::CREATE);
        
        foreach ($documents as $document) {
            $path = Storage::path(sprintf('public/%s', $document->path));
            
            if (file_exists($path)) {
                $extension = pathinfo($path)['extension'];
                $zip->addFile($path, $document->name . '.' . $extension);
            }
        }
        
        $zip->close();
        
        return config('app.url') . str_replace(storage_path('app/public'), '', $zipfilename);
    }
}
