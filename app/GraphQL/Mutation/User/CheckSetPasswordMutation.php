<?php

namespace App\GraphQL\Mutation\User;

use App\Repositories\Interfaces\UserRepositoryInterface;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;

class CheckSetPasswordMutation extends Mutation
{
    protected $user;
    
    protected $attributes = [
        'name'        => 'CheckSetPassword',
        'description' => 'Проверка, может ли пользователь установить пароль',
    ];
    
    
    public function __construct($attributes = [], UserRepositoryInterface $userRepository = null)
    {
        parent::__construct($attributes);
        $this->user = $userRepository;
    }
    
    
    public function type()
    {
        return Type::boolean();
    }
    
    
    public function args()
    {
        return [
            'hash'  => [
                'name'  => 'hash',
                'type'  => Type::nonNull(Type::string()),
                'rules' => ['max:255'],
            ],
            'email' => [
                'name'  => 'email',
                'type'  => Type::nonNull(Type::string()),
                'rules' => ['max:255|email'],
            ],
        ];
    }
    
    
    public function resolve($root, $args)
    {
        return $this->user->checkHash($args['email'], $args['hash']);
    }
}
