<?php

namespace App\GraphQL\Mutation\User;

use GraphQL\Error\UserError;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Repositories\Interfaces\UserRepositoryInterface;

class SetPasswordMutation extends Mutation
{
    protected $user = null;
    
    protected $attributes = [
        'name'        => 'SetPassword',
        'description' => 'Установка пароля для пользователя',
    ];
    
    
    public function __construct($attributes = [], UserRepositoryInterface $userRepository = null)
    {
        parent::__construct($attributes);
        $this->user = $userRepository;
    }
    
    
    public function type()
    {
        return Type::string();
    }
    
    
    public function args()
    {
        return [
            'hash'       => [
                'name'  => 'hash',
                'type'  => Type::nonNull(Type::string()),
                'rules' => ['max:255'],
            ],
            'email'      => [
                'name'  => 'email',
                'type'  => Type::nonNull(Type::string()),
                'rules' => ['max:255|email'],
            ],
            'password'   => [
                'name'  => 'password',
                'type'  => Type::nonNull(Type::string()),
                'rules' => ['min:6'],
            ],
            'repassword' => [
                'name'  => 'repassword',
                'type'  => Type::nonNull(Type::string()),
                'rules' => ['min:6'],
            ],
        ];
    }
    
    
    public function resolve($root, $args)
    {
        if ($args['password'] !== $args['repassword']) {
            throw new UserError(trans('validation.confirmed', ['attribute' => 'password']));
        }
        
        $user = $this->user->getByHash($args['email'], $args['hash']);
        
        if (is_null($user)) {
            throw new UserError('User not found.');
        }
        
        $user->password = $args['password'];
        $user->password_hash = null;
        $user->is_active = true;
        $user->save();
        
        return $this->user->token($args['email'], $args['password']);
    }
}
