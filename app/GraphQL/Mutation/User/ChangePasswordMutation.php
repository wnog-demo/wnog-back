<?php

namespace App\GraphQL\Mutation\User;

use Hash;
use Auth;
use GraphQL;
use App\Models\User;
use GraphQL\Type\Definition\{ResolveInfo, Type};
use Rebing\GraphQL\Support\{Mutation, SelectFields};

class ChangePasswordMutation extends Mutation
{
    protected $attributes = [
        'name'        => 'ChangePassword',
        'description' => 'Смена пароля для пользователя',
    ];
    
    
    public function authorize(array $args)
    {
        return User::check();
    }
    
    
    public function type()
    {
        return Type::boolean();
    }
    
    
    public function args()
    {
        return [
            'password'      => [
                'name'  => 'password',
                'type'  => Type::nonNull(Type::string()),
                'rules' => ['min:6'],
            ],
            'newpassword'   => [
                'name'  => 'newpassword',
                'type'  => Type::nonNull(Type::string()),
                'rules' => ['min:6'],
            ],
            'renewpassword' => [
                'name'  => 'renewpassword',
                'type'  => Type::nonNull(Type::string()),
                'rules' => ['min:6'],
            ],
        ];
    }
    
    
    public function resolve($root, $args)
    {
        if ($args['newpassword'] !== $args['renewpassword']) {
            return false;
        }
        
        $user = Auth::user();
        
        if (! Hash::check($args['password'], $user->password)) {
            return false;
        }
        
        $user->password = $args['newpassword'];
        
        return $user->save();
    }
}
