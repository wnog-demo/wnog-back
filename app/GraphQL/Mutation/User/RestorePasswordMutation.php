<?php

namespace App\GraphQL\Mutation\User;

use App\Notifications\RestorePassword;
use App\Repositories\Interfaces\UserRepositoryInterface;
use GraphQL\Error\UserError;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;

class RestorePasswordMutation extends Mutation
{
    protected $user = null;
    
    protected $attributes = [
        'name'        => 'RestorePassword',
        'description' => 'Восстановление пароля',
    ];
    
    
    public function __construct($attributes = [], UserRepositoryInterface $userRepository = null)
    {
        parent::__construct($attributes);
        $this->user = $userRepository;
    }
    
    
    public function type()
    {
        return Type::boolean();
    }
    
    
    public function args()
    {
        return [
            'email' => [
                'name'  => 'email',
                'type'  => Type::nonNull(Type::string()),
                'rules' => ['max:255|email'],
            ],
        ];
    }
    
    
    public function resolve($root, $args)
    {
        $user = $this->user->getByEmail($args['email']);
        
        if (is_null($user)) {
            throw new UserError('User not found or not active.');
        }
        
        $user->password_hash = $user->generateHash();
        $user->notify(new RestorePassword());
        
        //        Mail::send('mail.restorepassword', ['user' => $user], function (Message $message) use ($user) {
        //            $message->to($user->email, $user->fullname())
        //                ->subject('Восстановление пароля в аккаунте eWNOG!')
        //                ->from('dev@nimax.ru', 'eWNOG');
        //        });
        
        return $user->save();
    }
}
