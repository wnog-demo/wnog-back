<?php

namespace App\GraphQL\Mutation\User;

use App\Models\User;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;

class IsAuthMutation extends Mutation
{
    protected $attributes = [
        'name'        => 'IsAuth',
        'description' => 'Проверка, авторизован ли пользователь',
    ];
    
    
    public function type()
    {
        return Type::boolean();
    }
    
    
    public function resolve()
    {
        return User::check();
    }
}
