<?php

namespace App\GraphQL\Mutation\User;

use GraphQL\Error\UserError;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use App\Repositories\Interfaces\UserRepositoryInterface;

class GetTokenMutation extends Mutation
{
    protected $user = null;
    
    protected $attributes = [
        'name'        => 'GetToken',
        'description' => 'Получение токена пользователя',
    ];
    
    
    public function __construct($attributes = [], UserRepositoryInterface $userRepository = null)
    {
        parent::__construct($attributes);
        $this->user = $userRepository;
    }
    
    
    public function type()
    {
        return Type::string();
    }
    
    
    public function args()
    {
        return [
            'email'    => [
                'name'  => 'email',
                'type'  => Type::nonNull(Type::string()),
                'rules' => ['max:255|email'],
            ],
            'password' => [
                'name'  => 'password',
                'type'  => Type::nonNull(Type::string()),
                'rules' => ['min:6'],
            ],
        ];
    }
    
    
    public function resolve($root, $args)
    {
        $token = $this->user->token($args['email'], $args['password']);
        
        if (empty($token)) {
            throw new UserError('User not found or not active.');
        }
        
        return $token;
    }
}
