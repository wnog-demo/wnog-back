<?php

namespace App\GraphQL\Type;

use App;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;
use App\Models\Contact;

class ContactType extends GraphQLType
{
    protected $attributes = [
        'name'        => 'Contact',
        'description' => 'Контактное лицо вног',
        'model'       => Contact::class,
    ];
    
    
    public function fields(): array
    {
        return [
            'uid'  => [
                'type'        => Type::nonNull(Type::string()),
                'description' => 'UID пользователя',
            ],
            'name' => [
                'type'        => Type::nonNull(Type::string()),
                'description' => 'Имя',
                'resolve'     => function (Contact $contact) {
                    return App::isLocale('en') && ! empty($contact->en_name) ? $contact->en_name : $contact->name;
                },
            ],
        ];
    }
}
