<?php

namespace App\GraphQL\Type;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class StagesType extends GraphQLType
{
    protected $attributes = [
        'name'        => 'Stages',
        'description' => 'Этапы таможенного оформления',
    ];
    
    
    public function fields()
    {
        return [
            'type'   => [
                'type'        => Type::string(),
                'description' => 'Этап',
            ],
            'number' => [
                'type'        => Type::string(),
                'description' => 'Номер декларации (для типа "declaration")',
            ],
            'date'   => [
                'type'        => Type::int(),
                'description' => 'Дата (для типа "declaration")',
            ],
            'list'   => [
                'type'        => Type::listOf(GraphQL::type('Stage')),
                'description' => 'Этапы прибытия груза',
            ],
        ];
    }
}

