<?php

namespace App\GraphQL\Type;

use App;
use GraphQL;
use App\Models\User;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class UserType extends GraphQLType
{
    protected $attributes = [
        'name'        => 'User',
        'description' => 'Пользователь',
        'model'       => User::class,
    ];
    
    
    public function fields(): array
    {
        return [
            'uid'    => [
                'type'        => Type::nonNull(Type::string()),
                'description' => 'UID пользователя',
            ],
            'role'   => [
                'type'        => GraphQL::type('UserRole'),
                'description' => 'Роль пользователя',
            ],
            'client' => [
                'type'        => GraphQL::type('Client'),
                'description' => 'Клиент (компания)',
            ],
            'name'   => [
                'type'        => Type::nonNull(Type::string()),
                'description' => 'Имя пользователя',
                'resolve'     => function (User $user) {
                    return App::isLocale('en') && ! empty($user->en_name) ? $user->en_name : $user->name;
                },
            ],
            'email'  => [
                'type'        => Type::string(),
                'description' => 'Адрес эл. почты',
            ],
            'gender' => [
                'type'        => Type::string(),
                'description' => 'Пол',
            ],
        ];
    }
}
