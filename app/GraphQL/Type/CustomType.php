<?php

namespace App\GraphQL\Type;

use App;
use App\Models\Custom;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class CustomType extends GraphQLType
{
    protected $attributes = [
        'name'        => 'Custom',
        'description' => 'Таможня',
        'model'       => Custom::class,
    ];
    
    
    public function fields()
    {
        return [
            'uid'      => [
                'type'        => Type::nonNull(Type::string()),
                'description' => 'UID таможни',
            ],
            'name'     => [
                'type'        => Type::nonNull(Type::string()),
                'description' => 'Короткое название таможни',
            ],
            'fullname' => [
                'type'        => Type::string(),
                'description' => 'Полное название таможни',
                'resolve'     => function (Custom $custom) {
                    return App::isLocale('en') && ! empty($custom->en_fullname) ? $custom->en_fullname : $custom->fullname;
                },
            ],
            'city' => [
                'type'        => Type::string(),
                'description' => 'Город',
                'resolve'     => function (Custom $custom) {
                    return App::isLocale('en') && ! empty($custom->en_city) ? $custom->en_city : $custom->city;
                },
            ],
        ];
    }
}

