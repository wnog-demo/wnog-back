<?php

namespace App\GraphQL\Type;

use App;
use App\Models\Client;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class ClientType extends GraphQLType
{
    protected $attributes = [
        'name'        => 'Client',
        'description' => 'Клиент (компания)',
        'model'       => Client::class,
    ];
    
    
    public function fields()
    {
        return [
            'uid'  => [
                'type'        => Type::nonNull(Type::string()),
                'description' => 'UID клиента',
            ],
            'name' => [
                'type'        => Type::nonNull(Type::string()),
                'description' => 'Короткое название клиента (компании)',
                'resolve'     => function (Client $client) {
                    return App::isLocale('en') && ! empty($client->en_name) ? $client->en_name : $client->name;
                },
            ],
        ];
    }
}

