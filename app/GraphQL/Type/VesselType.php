<?php

namespace App\GraphQL\Type;

use App\Models\Vessel;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class VesselType extends GraphQLType
{
    protected $attributes = [
        'name'        => 'Vessel',
        'description' => 'Карабли',
        'model'       => Vessel::class,
    ];
    
    
    public function fields()
    {
        return [
            'uid'  => [
                'type'        => Type::nonNull(Type::string()),
                'description' => 'UID карабля',
            ],
            'name' => [
                'type'        => Type::nonNull(Type::string()),
                'description' => 'Название карабля',
            ],
        ];
    }
}
