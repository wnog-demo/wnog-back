<?php

namespace App\GraphQL\Type;

use App\Models\UserRole;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class UserRoleType extends GraphQLType
{
    protected $attributes = [
        'name'        => 'UserRole',
        'description' => 'Роли пользователя',
        'model'       => UserRole::class,
    ];
    
    
    public function fields()
    {
        return [
            'id'   => [
                'type'        => Type::nonNull(Type::int()),
                'description' => 'ID роли',
            ],
            'name' => [
                'type'        => Type::nonNull(Type::string()),
                'description' => 'Название роли',
            ],
            'slug' => [
                'type'        => Type::nonNull(Type::string()),
                'description' => 'Символьный код роли',
            ],
        ];
    }
}
