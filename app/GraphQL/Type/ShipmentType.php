<?php

namespace App\GraphQL\Type;

use App;
use App\Models\ShipmentType as Shipment;
use GraphQL\Type\Definition\Type;
use Lang;
use Rebing\GraphQL\Support\Type as GraphQLType;

class ShipmentType extends GraphQLType
{
    protected $attributes = [
        'name'        => 'Shipment',
        'description' => 'Груз',
        'model'       => Shipment::class,
    ];
    
    
    public function fields()
    {
        return [
            'uid'          => [
                'type'        => Type::nonNull(Type::string()),
                'description' => 'UID груза',
            ],
            'title'        => [
                'type'        => Type::string(),
                'description' => 'Название груза',
                'resolve'     => function (Shipment $shipment) {
                    return App::isLocale('en') && ! empty($shipment->en_name) ? $shipment->en_name : $shipment->name;
                },
            ],
            'name'         => [
                'type'        => Type::nonNull(Type::string()),
                'description' => 'Название груза',
                'resolve'     => function (Shipment $shipment) {
                    return App::isLocale('en') && ! empty($shipment->en_name) ? $shipment->en_name : $shipment->name;
                },
            ],
            'hazard_class' => [
                'type'        => Type::string(),
                'description' => 'Класс опасности',
            ],
            'hazard_code'  => [
                'type'        => Type::string(),
                'description' => 'Код опасности',
            ],
            'quantity'     => [
                'type'        => Type::string(),
                'description' => 'Количество',
                'resolve'     => function (Shipment $shipment) {
                    if (! preg_match('/([a-zA-Z]+)/', $shipment->quantity)) {
                        return sprintf('%d %s', $shipment->quantity, Lang::choice('shipment.places', $shipment->quantity));
                    }
                    
                    return $shipment->quantity;
                },
            ],
            'weight'       => [
                'type'        => Type::string(),
                'description' => 'Вес',
                'resolve'     => function (Shipment $shipment) {
                    if (! preg_match('/([a-zA-Z]+)/', $shipment->weight)) {
                        return sprintf('%s%s', str_replace(' ', ',', $shipment->weight), Lang::get('shipment.weight'));
                    }
                    
                    return $shipment->weight;
                },
            ],
        ];
    }
}

