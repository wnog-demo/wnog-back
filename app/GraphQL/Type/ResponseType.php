<?php

namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class ResponseType extends GraphQLType
{
    const STATUS_SUCCESS = 'success';
    
    const STATUS_ERROR   = 'error';
    
    protected $attributes = [
        'name'        => 'Response',
        'description' => 'Ответ для мутаций',
    ];
    
    
    public function fields(): array
    {
        return [
            'status'  => [
                'type'        => Type::nonNull(Type::string()),
                'description' => 'статуст ответа, [success|error]',
            ],
            'message' => [
                'type'        => Type::string(),
                'description' => 'Сообщение ответа',
            ],
        ];
    }
    
    
    public static function result(bool $success, string $message = ''): array
    {
        return [
            'status'  => $success ? self::STATUS_SUCCESS : self::STATUS_ERROR,
            'message' => $message,
        ];
    }
}
