<?php

namespace App\GraphQL\Type;

use App;
use App\Models\Notification;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class NotificationType extends GraphQLType
{
    protected $attributes = [
        'name'        => 'Notification',
        'description' => 'Уведомления',
        'model'       => Notification::class,
    ];
    
    
    public function fields(): array
    {
        return [
            'id'    => [
                'type'        => Type::nonNull(Type::int()),
                'description' => 'ID уведомления',
            ],
            'name'  => [
                'type'        => Type::nonNull(Type::string()),
                'description' => 'Название уведомления',
                'resolve'     => function ($notification) {
                    return App::isLocale('en') && ! empty($notification->en_name)
                        ? $notification->en_name : $notification->name;
                },
            ],
            'slug'  => [
                'type'        => Type::nonNull(Type::string()),
                'description' => 'Символьный код',
            ],
            'value' => [
                'type'        => Type::boolean(),
                'description' => 'Значение',
            ],
        ];
    }
}
