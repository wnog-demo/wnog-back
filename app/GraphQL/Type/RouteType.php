<?php

namespace App\GraphQL\Type;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class RouteType extends GraphQLType
{
    protected $attributes = [
        'name'        => 'Route',
        'description' => 'Маршрут',
    ];
    
    
    public function fields()
    {
        return [
            'type'   => [
                'type'        => Type::nonNull(Type::string()),
                'description' => 'Тип транспорта',
            ],
            'number' => [
                'type'        => Type::string(),
                'description' => 'Номер',
            ],
            'list'   => [
                'type'        => Type::listOf(GraphQL::type('Place')),
                'description' => 'Список точек',
            ],
        ];
    }
}

