<?php

namespace App\GraphQL\Type;

use App;
use App\Models\Stage\Transportation;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class TransportationType extends GraphQLType
{
    protected $attributes = [
        'name'        => 'Transportation',
        'description' => 'Перевозка',
        'model'       => Transportation::class,
    ];
    
    
    public function fields()
    {
        return [
            'id'      => [
                'type'        => Type::nonNull(Type::int()),
                'description' => 'ID статуса',
            ],
            'title'   => [
                'type'        => Type::nonNull(Type::string()),
                'description' => 'Заголовок перевозки',
                'resolve'     => function (Transportation $model) {
                    return $model->title();
                },
            ],
            'slug'    => [
                'type'        => Type::string(),
                'description' => 'Символьный код',
                'resolve'     => function (Transportation $model) {
                    return $model->name;
                },
            ],
            'comment' => [
                'type'        => Type::string(),
                'description' => 'Комментарий к статусу',
                'resolve'     => function (Transportation $model) {
                    return App::isLocale('en') && ! empty($model->en_comment) ? $model->en_comment : $model->comment;
                },
            ],
            'date'    => [
                'type'        => Type::int(),
                'description' => 'Дата',
                'resolve'     => function (Transportation $model): ?int {
                    return $model->date_status && $model->date_status->getTimestamp() > 0 ? $model->date_status->getTimestamp() : null;
                },
            ],
        ];
    }
}

