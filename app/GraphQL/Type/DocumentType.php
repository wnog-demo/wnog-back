<?php

namespace App\GraphQL\Type;

use App\Models\Document;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class DocumentType extends GraphQLType
{
    protected $attributes = [
        'name'        => 'Document',
        'description' => 'Документ ТО',
        'model'       => Document::class,
    ];
    
    
    public function fields()
    {
        return [
            'uid'  => [
                'type'        => Type::string(),
                'description' => 'UID места отправки-доставки',
            ],
            'name' => [
                'type'        => Type::string(),
                'description' => 'Название документа',
            ],
            'path' => [
                'type'        => Type::string(),
                'description' => 'Путь до документа',
                'resolve'     => function (Document $model) {
                    return asset(sprintf('storage%s', $model->path));
                },
            ],
            'date' => [
                'type'        => Type::int(),
                'description' => 'Дата',
                'resolve'     => function (Document $model): ?int {
                    return $model->date && $model->date->getTimestamp() > 0 ? $model->date->getTimestamp() : null;
                },
            ],
        ];
    }
}

