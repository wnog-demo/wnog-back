<?php

namespace App\GraphQL\Type;

use App;
use App\Models\Product;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class ProductType extends GraphQLType
{
    protected $attributes = [
        'name'        => 'Product',
        'description' => 'Продуктовая линейка',
        'model'       => Product::class,
    ];
    
    
    public function fields()
    {
        return [
            'uid'  => [
                'type'        => Type::nonNull(Type::string()),
                'description' => 'UID продукта',
            ],
            'name' => [
                'type'        => Type::nonNull(Type::string()),
                'description' => 'Название продуктовой линейки',
                'resolve'     => function (Product $product) {
                    return App::isLocale('en') && ! empty($product->en_name) ? $product->en_name : $product->name;
                },
            ],
        ];
    }
}

