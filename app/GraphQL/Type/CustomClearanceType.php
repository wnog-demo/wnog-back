<?php

namespace App\GraphQL\Type;

use App\Models\CustomClearance;
use App\Models\Declaration;
use App\Models\Document;
use App\Models\Stage\GreenLight;
use GraphQL;
use GraphQL\Type\Definition\Type;
use Illuminate\Support\Collection;
use Rebing\GraphQL\Support\Type as GraphQLType;

class CustomClearanceType extends GraphQLType
{
    protected $attributes = [
        'name'        => 'CustomClearance',
        'description' => 'Таможенное оформление',
        'model'       => CustomClearance::class,
    ];
    
    
    public function fields()
    {
        return [
            'uid'             => [
                'type'        => Type::nonNull(Type::string()),
                'description' => 'UID Таможенного оформления',
            ],
            'type'            => [
                'type'        => Type::string(),
                'description' => 'Тип таможенного оформления (export|import)',
            ],
            'date'            => [
                'type'        => Type::int(),
                'description' => 'Дата создания',
                'resolve'     => function (CustomClearance $model) {
                    return $model->date->getTimestamp();
                },
            ],
            'reference'       => [
                'type'        => Type::string(),
                'description' => 'Уникальный id груза',
            ],
            'user'            => [
                'type'        => GraphQL::type('User'),
                'description' => 'Клиент',
            ],
            'contact'         => [
                'type'        => GraphQL::type('Contact'),
                'description' => 'Контактное лицо вног',
            ],
            'customer'        => [
                'type'        => GraphQL::type('Client'),
                'description' => 'Клиент',
            ],
            'custom'          => [
                'type'        => GraphQL::type('Custom'),
                'description' => 'Таможня',
            ],
            'shipper'         => [
                'type'        => GraphQL::type('Shipper'),
                'description' => 'Отправитель',
            ],
            'consignee'       => [
                'type'        => GraphQL::type('Shipper'),
                'description' => 'Получатель',
            ],
            'product'         => [
                'type'        => GraphQL::type('Product'),
                'description' => 'Продуктовая линейка',
            ],
            'documents'       => [
                'type'        => Type::listOf(GraphQL::type('DocumentCategory')),
                'description' => 'Документы',
                'resolve'     => function (CustomClearance $model) {
                    $documents = [];
                    
                    $model->documents->map(function (Document $document) use (&$documents) {
                        if (empty($documents[$document->category_name]['name'])) {
                            $documents[$document->category_name]['name'] = $document->category_name;
                        }
                        $documents[$document->category_name]['documents'][] = $document;
                    });
                    
                    return array_values($documents);
                },
            ],
            'stages'          => [
                'type'        => Type::listOf(GraphQL::type('Stages')),
                'description' => 'Этапы ТО',
                'is_relation' => false,
                'resolve'     => function (CustomClearance $model) {
                    $stages = [];
                    
                    if ($model->greenlights->isNotEmpty()) {
                        $stages[] = [
                            'type' => 'greenlight',
                            'list' => (function (Collection $greenlights) { // сортировк по ключам в нужном порядке
                                $sort = ['request-received' => 0, 'not-confirmed' => 1, 'confirmed' => 2];
                                
                                return $greenlights->sort(function (GreenLight $a, GreenLight $b) use ($sort) {
                                    return $sort[$a->name] <=> $sort[$b->name];
                                });
                            })($model->greenlights),
                        ];
                    }
                    
                    if ($model->comings->isNotEmpty()) {
                        $stages[] = [
                            'type' => 'coming',
                            'list' => $model->comings,
                        ];
                    }
                    
                    if ($model->declarations->isNotEmpty()) {
                        $model->declarations->each(function (Declaration $declaration) use (&$stages) {
                            $stages[] = [
                                'type'   => 'declaration',
                                'number' => $declaration->number,
                                'date'   => $declaration->date && $declaration->date->getTimestamp() > 0 ? $declaration->date->getTimestamp() : null,
                                'list'   => $declaration->stages,
                            ];
                        });
                    }
                    
                    return $stages;
                },
            ],
            'transportations' => [
                'type'        => Type::listOf(GraphQL::type('Transportation')),
                'description' => 'Статус перевозки',
                'is_relation' => false,
                'resolve'     => function (CustomClearance $model) {
                    return $model->transportations;
                },
            ],
            'route'           => [
                'type'        => GraphQL::type('Route'),
                'description' => 'Маршрут',
                'is_relation' => false,
                'resolve'     => function (CustomClearance $model) {
                    return $model->route();
                },
            ],
            'shipments'       => [
                'type'        => GraphQL::type('Shipment'),
                'description' => 'Груз',
                'resolve'     => function (CustomClearance $model) {
                    return $model->shipments->first();
                },
            ],
            'delivery'        => [
                'type'        => GraphQL::type('Place'),
                'description' => 'Место доставки',
                'is_relation' => false,
                'resolve'     => function (CustomClearance $model) {
                    return $model->place_of_delivery;
                },
            ],
            'vessel'          => [
                'type'        => GraphQL::type('Vessel'),
                'description' => 'Карабль',
            ],
        ];
    }
}

