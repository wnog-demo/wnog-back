<?php

namespace App\GraphQL\Type;

use App;
use App\Contracts\StageInterface;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class StageType extends GraphQLType
{
    protected $attributes = [
        'name'        => 'Stage',
        'description' => 'Этап таможенного оформления',
    ];
    
    
    public function fields()
    {
        return [
            'id'      => [
                'type'        => Type::id(),
                'description' => 'ID этапа',
                'resolve'     => function (StageInterface $stage) {
                    return $stage->name . '-' . $stage->id;
                },
            ],
            'title'   => [
                'type'        => Type::nonNull(Type::string()),
                'description' => 'Заголовок этапа',
                'resolve'     => function (StageInterface $stage) {
                    return $stage->title();
                },
            ],
            'comment' => [
                'type'        => Type::string(),
                'description' => 'Комментарий к этапу',
                'resolve'     => function (StageInterface $stage) {
                    return App::isLocale('en') && ! empty($stage->en_comment) ? $stage->en_comment : $stage->comment;
                },
            ],
            'date'    => [
                'type'        => Type::int(),
                'description' => 'Дата статуса',
                'resolve'     => function (StageInterface $stage) {
                    return $stage->date_status && $stage->date_status->getTimestamp() > 0 ? $stage->date_status->getTimestamp() : 0;
                },
            ],
            'status'  => [
                'type'        => Type::string(),
                'description' => 'Статус',
                'is_relation' => false,
                'resolve'     => function (StageInterface $stage) {
                    return $stage->status();
                },
            ],
        ];
    }
}

