<?php

namespace App\GraphQL\Type;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class DocumentCategoryType extends GraphQLType
{
    protected $attributes = [
        'name'        => 'DocumentCategory',
        'description' => 'Документы ТО',
    ];
    
    
    public function fields()
    {
        return [
            'name'      => [
                'type'        => Type::string(),
                'description' => 'Название категории',
            ],
            'documents' => [
                'type'        => Type::listOf(GraphQL::type('Document')),
                'description' => 'Документы',
            ],
        ];
    }
}

