<?php

namespace App\GraphQL\Type;

use App;
use App\Models\Shipper;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class ShipperType extends GraphQLType
{
    protected $attributes = [
        'name'        => 'Shipper',
        'description' => 'Отправители-получатели',
        'model'       => Shipper::class,
    ];
    
    
    public function fields()
    {
        return [
            'uid'     => [
                'type'        => Type::nonNull(Type::string()),
                'description' => 'UID',
            ],
            'name'    => [
                'type'        => Type::nonNull(Type::string()),
                'description' => 'Короткое название таможни',
                'resolve'     => function (Shipper $shipper) {
                    return App::isLocale('en') && ! empty($shipper->en_name) ? $shipper->en_name : $shipper->name;
                },
            ],
            'address' => [
                'type'        => Type::string(),
                'description' => 'Адрес',
            ],
        ];
    }
}

