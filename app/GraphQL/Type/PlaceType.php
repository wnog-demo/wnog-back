<?php

namespace App\GraphQL\Type;

use App;
use App\Models\Place;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class PlaceType extends GraphQLType
{
    protected $attributes = [
        'name'        => 'Place',
        'description' => 'Места отправки-доставки',
        'model'       => Place::class,
    ];
    
    
    public function fields()
    {
        return [
            'uid'     => [
                'type'        => Type::string(),
                'description' => 'UID места отправки-доставки',
            ],
            'title'   => [
                'type'        => Type::string(),
                'description' => 'Подпись места отправки-доставки',
            ],
            'name'    => [
                'type'        => Type::string(),
                'description' => 'Название места',
                'resolve'     => function (Place $place) {
                    return App::isLocale('en') && ! empty($place->en_name) ? $place->en_name : $place->name;
                },
            ],
            'address' => [
                'type'        => Type::string(),
                'description' => 'Адрес',
            ],
            'type'    => [
                'type'        => Type::string(),
                'description' => 'Тип объекта',
            ],
        ];
    }
}

