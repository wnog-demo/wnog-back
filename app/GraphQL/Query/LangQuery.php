<?php

namespace App\GraphQL\Query;

use App;
use App\Models\User;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;

class LangQuery extends Query
{
    protected $attributes = [
        'name'        => 'Lang',
        'description' => 'Языковые фразы',
    ];
    
    
    public function authorize(array $args)
    {
        return User::check();
    }
    
    
    public function type()
    {
        return Type::string();
    }
    
    
    public function args()
    {
        return [];
    }
    
    
    public function resolve($root, $args)
    {
        $lang = $this->lang();
        $files = array_map(function ($item) {
            return str_replace('.php', '', $item);
        }, array_diff(scandir(base_path('resources/lang/' . $lang . '/front')), ['.', '..']));
        
        $data = [];
        
        foreach ($files as $file) {
            $data[$file] = include base_path('resources/lang/' . $lang . '/front/' . $file . '.php');
        }
        
        return json_encode($data, JSON_UNESCAPED_UNICODE);
    }
    
    
    protected function lang()
    {
        return App::getLocale();
    }
}
