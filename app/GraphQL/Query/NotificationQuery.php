<?php

namespace App\GraphQL\Query;

use Auth;
use GraphQL;
use App\Models\User;
use App\Models\Notification;
use GraphQL\Type\Definition\{Type, ResolveInfo};
use Rebing\GraphQL\Support\{Query, SelectFields};

class NotificationQuery extends Query
{
    protected $attributes = [
        'name'        => 'Notification',
        'description' => 'Уведомления',
    ];
    
    
    public function authorize(array $args)
    {
        return User::check();
    }
    
    
    public function type()
    {
        return Type::listOf(GraphQL::type('Notification'));
    }
    
    
    public function args()
    {
        return [];
    }
    
    
    public function resolve($root, $args, SelectFields $fields, ResolveInfo $info)
    {
        $uid = Auth::id();
        
        if (empty($uid)) {
            return null;
        }
        
        return Notification::valueByUser($uid);
    }
}
