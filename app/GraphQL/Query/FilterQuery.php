<?php

namespace App\GraphQL\Query;

use App;
use App\Models\{Custom, User};
use App\Models\Stage\{Coming, Declaration, GreenLight, Transportation};
use DB;
use GraphQL\Type\Definition\Type;
use Illuminate\Support\Str;
use Rebing\GraphQL\Support\Query;

class FilterQuery extends Query
{
    protected $attributes = [
        'name'        => 'Filter',
        'description' => 'Фильтр для таможенного оформления, return JSON',
    ];
    
    
    public function authorize(array $args)
    {
        return User::check();
    }
    
    
    public function type()
    {
        return Type::string();
    }
    
    
    public function args()
    {
        return [];
    }
    
    
    public function resolve($root, $args)
    {
        return json_encode([
            'customs'         => self::customs(),
            'types'           => self::types(),
            'comings'         => self::comings(),
            'greenlights'     => self::greenlights(),
            'declarations'    => self::declarations(),
            'transportations' => self::transportations(),
        ], JSON_UNESCAPED_UNICODE);
    }
    
    
    protected static function customs()
    {
        $uids = DB::table('custom_clearances')
            ->select(['custom_uid'])
            ->whereNotNull('custom_uid')
            ->distinct()
            ->get()
            ->map(function ($row) {
                return $row->custom_uid;
            });
        
        return Custom::whereIn('uid', $uids)
            ->get()
            ->map(function (Custom $custom) {
                return [
                    'title' => App::isLocale('en') && ! empty($custom->name) ? $custom->name : $custom->fullname,
                    'value' => $custom->uid,
                ];
            })
            ->toArray();
    }
    
    
    protected static function types()
    {
        return App::isLocale('en')
            ? [['title' => 'Export', 'value' => 'export'], ['title' => 'Import', 'value' => 'import']]
            : [['title' => 'Экспорт', 'value' => 'export'], ['title' => 'Импорт', 'value' => 'import']];
    }
    
    
    protected static function greenlights()
    {
        return GreenLight::select('name')
            ->distinct()
            ->get()
            ->map(function (GreenLight $greenlight) {
                return [
                    'title' => $greenlight->title(),
                    'value' => $greenlight->name,
                ];
            })
            ->toArray();
    }
    
    
    protected static function declarations()
    {
        $items = $declarations = [];
        $sort = [
            'registered',
            'assigned-inspection',
            'inspection-completed',
            'conditional-release',
            'released',
            'refusal-to-issue',
        ];
        
        Declaration::select('name')
            ->distinct()
            ->get()
            ->each(function (Declaration $declaration) use (&$declarations) {
                $declarations[$declaration->name] = [
                    'title' => $declaration->title(),
                    'value' => $declaration->name,
                ];
            })
            ->toArray();
        
        foreach ($sort as $s) {
            if (isset($declarations[$s])) {
                $items[] = $declarations[$s];
            }
        }
        
        return [
            'items' => $items,
        ];
    }
    
    
    protected static function comings()
    {
        return Coming::distinct()
            ->select('name')
            ->get()
            ->map(function (Coming $coming) {
                return [
                    'title' => Str::upper($coming->name),
                    'value' => $coming->name,
                ];
            })
            ->toArray();
    }
    
    
    protected static function transportations()
    {
        return Transportation::select('name')
            ->distinct()
            ->get()
            ->map(function (Transportation $transportation) {
                return [
                    'title' => $transportation->title(),
                    'value' => $transportation->name,
                ];
            })
            ->toArray();
    }
}
