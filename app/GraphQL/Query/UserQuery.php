<?php

namespace App\GraphQL\Query;

use App\Repositories\UserRepository;
use Auth;
use GraphQL;
use App\Models\User;
use GraphQL\Type\Definition\{Type, ResolveInfo};
use Rebing\GraphQL\Support\{Query, SelectFields};

class UserQuery extends Query
{
    protected $user = null;
    
    protected $attributes = [
        'name'        => 'User',
        'description' => 'Пользователь',
    ];
    
    
    public function __construct($attributes = [], UserRepository $userRepository = null)
    {
        parent::__construct($attributes);
        $this->user = $userRepository;
    }
    
    
    public function authorize(array $args)
    {
        return User::check();
    }
    
    
    public function type()
    {
        return GraphQL::type('User');
    }
    
    
    public function args()
    {
        return [
            'uid' => [
                'name' => 'uid',
                'type' => Type::string(),
            ],
        ];
    }
    
    
    public function resolve($root, $args, SelectFields $fields, ResolveInfo $info)
    {
        $uid = isset($args['uid']) ? $args['uid'] : Auth::id();
        
        return $this->user->getByUID($uid);
    }
}
