<?php

namespace App\GraphQL\Query;

use App\Models\{CustomClearance, CustomClearanceLog, Stage\Declaration, Stage\GreenLight, Stage\Transportation, User, UserRole};
use Auth;
use GraphQL;
use GraphQL\Error\UserError;
use GraphQL\Type\Definition\Type;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Carbon;
use Rebing\GraphQL\Support\Query;

class CustomClearanceQuery extends Query
{
    protected $attributes = [
        'name'        => 'CustomClearance',
        'description' => 'Таможенные оформления',
    ];
    
    
    public function authorize(array $args)
    {
        return User::check();
    }
    
    
    public function type()
    {
        return Type::listOf(GraphQL::type('CustomClearance'));
    }
    
    
    public function args()
    {
        return [
            'uid'             => [
                'name' => 'uid',
                'type' => Type::string(),
            ],
            'limit'           => [
                'name' => 'limit',
                'type' => Type::int(),
            ],
            'offset'          => [
                'name' => 'offset',
                'type' => Type::int(),
            ],
            'query'           => [
                'name'        => 'query',
                'type'        => Type::string(),
                'description' => 'Текст запроса',
            ],
            'type'            => [
                'name'        => 'type',
                'type'        => Type::listOf(Type::string()),
                'description' => 'type: export|import',
            ],
            'customs'         => [
                'name'        => 'customs',
                'type'        => Type::listOf(Type::string()),
                'description' => 'Таможни',
            ],
            'comings'         => [
                'name'        => 'comings',
                'type'        => Type::listOf(Type::string()),
                'description' => 'status.comings: ata|eta',
            ],
            'greenlights'     => [
                'name'        => 'greenlights',
                'type'        => Type::listOf(Type::string()),
                'description' => 'status.greenlights:request-received|confirmed|not-confirmed|registered|released',
            ],
            'transportations' => [
                'name'        => 'transportations',
                'type'        => Type::listOf(Type::string()),
                'description' => 'status.transportations:waiting|confirmed|not-confirmed|cargo-shipped',
            ],
            'declarations'    => [
                'name'        => 'declarations',
                'type'        => Type::listOf(Type::string()),
                'description' => 'status.declarations:registered|assigned-inspection|inspection-completed|conditional-release|released|refusal-to-issue|pre-declaration',
            ],
            'ata'             => [
                'name'        => 'ata',
                'type'        => Type::string(),
                'description' => 'ata',
            ],
            'eta'             => [
                'name'        => 'eta',
                'type'        => Type::string(),
                'description' => 'eta',
            ],
            'is_archive'      => [
                'name'        => 'is_archive',
                'type'        => Type::boolean(),
                'description' => 'is archive',
            ],
        ];
    }
    
    
    public function resolve($root, $args)
    {
        /** @var User $user */
        $user = Auth::user();
        $customClearances = CustomClearance::with(CustomClearance::$load);
        
        if (is_null($user->role)) {
            throw new UserError('User role not found.');
        }
        
        switch ($user->role->id) {
            case UserRole::ROLE_MANAGER:
                if (empty($user->client)) {
                    throw new UserError('User client not found.');
                }
                
                $customClearances->where('customer_uid', $user->client->uid);
                break;
            case UserRole::ROLE_COORDINATOR:
                $customClearances->where('user_uid', $user->uid);
                break;
            default:
                throw new UserError('Invalid user role.');
        }
        
        if (isset($args['uid'])) {
            $customClearances = $customClearances->where('uid', $args['uid'])
                ->limit(1)
                ->get();
        } else {
            // фильтр с текстом
            if (! empty($args['query'])) {
                $customClearances->where('reference', 'like', '%' . $args['query'] . '%');
            }
            
            // фильтр для типа
            if (! empty($args['type'])) {
                $customClearances->whereIn('type', $args['type']);
            }
            
            // фильтр для green light
            if (! empty($args['greenlights']) && $values = $args['greenlights']) {
                $items = $ids = [];
                
                GreenLight::select(['id', 'custom_clearance_uid', 'name'])
                    ->orderBy('id')
                    ->get()
                    ->each(function ($status) use (&$items) {
                        $items[$status->custom_clearance_uid][$status->id] = $status->name;
                    });
                
                foreach ($items as $item) {
                    $id = key($item);
                    $item = array_values($item);
                    
                    if (in_array($item[count($item) - 1], $values)) {
                        $ids[] = $id;
                    }
                }
                
                $customClearances->whereHas('greenlights', function (Builder $builder) use ($ids) {
                    return $builder->whereIn('id', $ids);
                });
            }
            
            // фильтр для перевозки
            if (! empty($args['transportations']) && $values = $args['transportations']) {
                $items = $ids = [];
                
                Transportation::select(['id', 'custom_clearance_uid', 'name'])
                    ->orderBy('id')
                    ->get()
                    ->each(function ($status) use (&$items) {
                        $items[$status->custom_clearance_uid][$status->id] = $status->name;
                    });
                
                foreach ($items as $k => $item) {
                    $id = key($item);
                    $item = array_values($item);
                    
                    if (in_array($item[count($item) - 1], $values)) {
                        $ids[] = $id;
                    }
                }
                
                $customClearances->whereHas('transportations', function (Builder $builder) use ($ids) {
                    return $builder->whereIn('id', $ids);
                });
            }
            
            // фильтр для декларации
            if (! empty($args['declarations']) && $values = $args['declarations']) {
                $customClearances->whereHas('declarations', function (Builder $builder) use ($values) {
                    return $builder->whereHas('stages', function (Builder $builder) use ($values) {
                        $items = $ids = [];
                        
                        Declaration::select(['id', 'declaration_uid', 'name'])
                            ->orderBy('id')
                            ->get()
                            ->each(function ($status) use (&$items) {
                                $items[$status->declaration_uid][$status->id] = $status->name;
                            });
                        
                        foreach ($items as $item) {
                            $id = key($item);
                            $item = array_values($item);
                            
                            if (in_array($item[count($item) - 1], $values)) {
                                $ids[] = $id;
                            }
                        }
                        
                        return $builder->whereIn('id', $ids);
                    });
                });
            } elseif (! empty($args['ata']) || ! empty($args['eta'])) {
                $customClearances->doesntHave('declarations');
            }
            
            // фильтр для таможни
            if (! empty($args['customs']) && $values = $args['customs']) {
                $customClearances->whereHas('custom', function (Builder $builder) use ($values) {
                    return $builder->whereIn('uid', $values);
                });
            }
            
            // фильтр для ata
            if (! empty($args['ata']) && $values = json_decode($args['ata'], true)) {
                $customClearances->whereHas('comings', function (Builder $builder) use ($values) {
                    if (empty($args['eta'])) {
                        $builder->where('name', '!=', 'eta');
                    }
                    
                    if ($values['from'] === 0 && $values['to'] === 0) {
                        return $builder->where('name', 'ata');
                    } elseif (! empty($values['from']) && ! empty($values['to'])) {
                        return $builder->where('name', 'ata')
                            ->whereBetween('date', [
                                Carbon::createFromTimestamp($values['from']),
                                Carbon::createFromTimestamp($values['to']),
                            ]);
                    } elseif (! empty($values['from']) && empty($values['to'])) {
                        return $builder->where('name', 'ata')
                            ->where('date', '>=', Carbon::createFromTimestamp($values['from']));
                    } elseif (empty($values['from']) && ! empty($values['to'])) {
                        return $builder->where('name', 'ata')
                            ->where('date', '<=', Carbon::createFromTimestamp($values['to']));
                    }
                    
                    return $builder;
                });
            } elseif (! empty($args['greenlights'])) {
                $customClearances->doesntHave('comings');
            }
            
            // фильтр для eta
            if (! empty($args['eta']) && $values = json_decode($args['eta'], true)) {
                $customClearances->whereHas('comings', function (Builder $builder) use ($values) {
                    if ($values['from'] === 0 && $values['to'] === 0) {
                        return $builder->where('name', 'eta');
                    } elseif ($values['from'] > 0 && $values['to'] > 0) {
                        return $builder->where('name', 'eta')
                            ->whereBetween('date', [
                                Carbon::createFromTimestamp($values['from']),
                                Carbon::createFromTimestamp($values['to']),
                            ]);
                    } elseif (! empty($values['from']) && empty($values['to'])) {
                        return $builder->where('name', 'eta')
                            ->where('date', '>=', Carbon::createFromTimestamp($values['from']));
                    } elseif (empty($values['from']) && ! empty($values['to'])) {
                        return $builder->where('name', 'eta')
                            ->where('date', '<=', Carbon::createFromTimestamp($values['to']));
                    }
                    
                    return $builder;
                });
            } elseif (! empty($args['greenlights'])) {
                $customClearances->doesntHave('comings');
            }
            
            $transportations = function (Builder $builder) {
                return $builder->where(function (Builder $q) {
                    // если есть статус cargo-shipped и date_status == null
                    return $q->where('name', 'cargo-shipped')
                        ->whereNull('date_status');
                })
                    ->orWhere(function (Builder $q) {
                        // или если статус cargo-shipped и date_status <= (now() - oneWeek())
                        return $q->where('name', 'cargo-shipped')
                            ->whereDate('date_status', '<=', Carbon::now()
                                ->subWeek());
                    });
            };
            
            if (empty($args['query']) && isset($args['is_archive']) && $args['is_archive'] == true) {
                $customClearances->whereHas('transportations', function (Builder $builder) use ($transportations) {
                    return $transportations($builder);
                });
            } elseif (empty($args['query'])) {
                $customClearances->doesntHave('transportations', 'and', function (Builder $builder) use ($transportations) {
                    return $transportations($builder);
                });
            }
            
            $customClearances = $customClearances->limit($args['limit'] ?? 50)
                ->offset($args['offset'] ?? 0)
                ->orderByDesc('updated_at')
                ->get();
            
            if (! empty($args['ata']) && empty($args['eta'])) {
                $customClearances = $customClearances->filter(function (CustomClearance $clearance) {
                    $coming = $clearance->comings->last();
                    
                    return $coming->name == 'ata';
                });
            }
            
            if (! empty($args['eta']) && empty($args['ata'])) {
                $customClearances = $customClearances->filter(function (CustomClearance $clearance) {
                    $coming = $clearance->comings->last();
                    
                    return $coming->name == 'eta';
                });
            }
        }
        
        CustomClearanceLog::create([
            'user_uid' => $user->uid,
            'action'   => json_encode($args, JSON_UNESCAPED_UNICODE),
            'date'     => Carbon::now(),
        ]);
        
        return $customClearances;
    }
}
