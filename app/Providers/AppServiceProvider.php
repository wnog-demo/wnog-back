<?php

namespace App\Providers;

use URL;
use Illuminate\Support\ServiceProvider;
use Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        if ($this->app->environment() == 'local') {
            $this->app->register(IdeHelperServiceProvider::class);
        } elseif ($this->app->environment() == 'production') {
            URL::forceScheme('https');
        }
    }
    
    
    public function boot(): void
    {
    }
}
