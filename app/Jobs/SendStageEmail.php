<?php

namespace App\Jobs;

use App;
use App\Contracts\StageInterface;
use App\Models\Notification;
use App\Models\Stage\Declaration;
use App\Models\User;
use App\Notifications\NewStatus;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\{InteractsWithQueue, SerializesModels};

class SendStageEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    protected $statuses;
    
    
    public function __construct(array $statuses)
    {
        $this->statuses = $statuses;
    }
    
    
    public function handle(): void
    {
        if (! is_array($this->statuses) || App::environment() != 'production') {
            return;
        }
        
        $this->statuses = array_reverse($this->statuses);
        $items = $users = [];
        
        foreach ($this->statuses as $item) {
            /** @var StageInterface $stage */
            $stage = $item['stage'];
            
            if (empty($stage)) {
                return;
            }
            
            $slug = $this->slug($item['type'], $stage->name);
            
            if (is_null($slug)) {
                continue;
            }
            
            $items[] = [
                'slug'  => $slug,
                'stage' => $stage,
            ];
            
            if ($item['type'] == 'declaration') {
                /** @var Declaration $stage */
                if ($user = $stage->declaration->custom_clearance->user) {
                    $users[$user->uid] = $user;
                }
            } elseif ($item['type'] == 'document') {
                if ($user = $stage->custom_clearance->user) {
                    $users[$user->uid] = $user;
                }
            } else {
                if ($user = $stage->customClearance->user) {
                    $users[$user->uid] = $user;
                }
            }
        }
        
        if (empty($users)) {
            return;
        }
        
        foreach (NewStatus::data($items, $users) as $userUid => $res) {
            /** @var User $user */
            if (isset($users[$userUid]) && $user = $users[$userUid]) {
                [$subject, $data] = $res;
                $user->notify(new NewStatus($subject, $data));
            }
        }
    }
    
    
    protected function slug(string $type, string $name): ?string
    {
        switch ($type) {
            case 'coming':
                return $this->coming($name);
            case 'transportation':
                return $this->transportation($name);
            case 'greenlight':
                return $this->greenlight($name);
            case 'declaration':
                return $this->declaration($name);
            case 'document':
                return $this->document($name);
            default:
                return null;
        }
    }
    
    
    protected function coming(string $name): ?string
    {
        switch ($name) {
            case 'ata':
                return 'coming-ata';
            default:
                return null;
        }
    }
    
    
    protected function transportation(string $name): ?string
    {
        switch ($name) {
            case 'cargo-shipped':
                return 'transportation-cargo-shipped';
            default:
                return null;
        }
    }
    
    
    protected function greenlight(string $name): ?string
    {
        switch ($name) {
            case 'request-received':
                return 'greenlight-request-received';
            case 'confirmed':
                return 'greenlight-confirmed';
            default:
                return null;
        }
    }
    
    
    protected function declaration(string $name): ?string
    {
        switch ($name) {
            case 'registered':
                return 'declaration-registered';
            case 'released':
                return 'declaration-released';
            case 'refusal-to-issue':
                return 'declaration-refusal';
            case 'assigned-inspection':
                return 'declaration-appointment';
            case 'inspection-completed':
                return 'declaration-completion';
            case 'pre-declaration':
                return 'declaration-shipped';
            default:
                return null;
        }
    }
    
    
    protected function document(string $name): ?string
    {
        return 'document-create';
    }
    
    
    protected static function notification($slug): ?Notification
    {
        return Notification::where('slug', $slug)
            ->where('is_active', true)
            ->first();
    }
}
