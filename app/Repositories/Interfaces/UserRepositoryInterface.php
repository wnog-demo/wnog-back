<?php

namespace App\Repositories\Interfaces;

use App\Models\User;

interface UserRepositoryInterface
{
    public function getByUID(string $uid): ?User;
    
    
    public function getByEmail(string $email): ?User;
    
    
    public function getByHash(string $email, string $hash): ?User;
    
    
    public function checkHash(string $email, string $hash): bool;
    
    
    /**
     * Токен пользователя для авторизации
     *
     * @param string $email    Адрес эл.почты
     * @param string $password Пароль
     *
     * @return string
     */
    
    public function token(string $email, string $password): string;
}
