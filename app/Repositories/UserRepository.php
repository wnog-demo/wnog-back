<?php

namespace App\Repositories;

use JWTAuth;
use App\Models\User;
use App\Repositories\Interfaces\UserRepositoryInterface;

class UserRepository implements UserRepositoryInterface
{
    public function getByUID(string $uid): ?User
    {
        $user = User::where('uid', $uid)
            ->where('is_active', true)
            ->limit(1);
        
        return $user->first();
    }
    
    
    public function getByEmail(string $email): ?User
    {
        $user = User::where('email', $email)
            ->where('is_active', true)
            ->limit(1);
        
        return $user->first();
    }
    
    
    public function getByHash(string $email, string $hash): ?User
    {
        $user = User::where('email', $email)
            ->where('password_hash', $hash)
            ->limit(1);
        
        return $user->first();
    }
    
    
    public function checkHash(string $email, string $hash): bool
    {
        return ! is_null($this->getByHash($email, $hash));
    }
    
    
    public function token(string $email, string $password): string
    {
        $credentials = [
            'email'    => $email,
            'password' => $password,
        ];
        
        $token = JWTAuth::attempt($credentials);
        /** @var User $user */
        $user = JWTAuth::user();
        
        return ! is_null($user) && $user->is_active && ! empty($token) ? $token : '';
    }
}
