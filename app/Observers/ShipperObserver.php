<?php

namespace App\Observers;

use Illuminate\Support\Str;
use App\Models\Shipper;

class ShipperObserver
{
    public function creating(Shipper $shipper)
    {
        if (empty($shipper->en_name)) {
            $shipper->en_name = Str::ascii($shipper->name);
        }
    }
}
