<?php

namespace App\Observers;

use App\Models\Document;
use App\Models\StatusMail;

class DocumentObserver
{
    public function created(Document $document)
    {
        StatusMail::create([
            'custom_clearance_uid' => $document->custom_clearance_uid,
            'status_id'            => 0,
            'document_uid'         => $document->uid,
            'type'                 => 'document',
        ]);
    }
}
