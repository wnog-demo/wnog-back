<?php

namespace App\Observers;

use App;
use Illuminate\Support\Str;
use App\Notifications\NewUser;
use App\Models\{User, Notification};

class UserObserver
{
    public function creating(User $user)
    {
        $user->password_hash = $user->generateHash();
        
        if (empty($user->en_name)) {
            $user->en_name = Str::ascii($user->name);
        }
    }
    
    
    public function created(User $user)
    {
        if (App::environment() == 'production') {
            $user->notify(new NewUser());
        }
        
        Notification::initForUser($user);
    }
}
