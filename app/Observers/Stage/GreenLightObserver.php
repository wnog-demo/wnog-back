<?php

namespace App\Observers\Stage;

use App\Contracts\StageInterface;
use App\Models\CustomClearance;
use App\Models\StatusMail;

class GreenLightObserver
{
    public function created(StageInterface $status)
    {
        StatusMail::create([
            'custom_clearance_uid' => $status->custom_clearance_uid,
            'status_id'            => $status->id,
            'type'                 => 'greenlight',
        ]);
        
        // CustomClearance::updateDate($status->custom_clearance_uid);
    }
    
    
    public function updated(StageInterface $status)
    {
        // CustomClearance::updateDate($status->custom_clearance_uid);
    }
}
