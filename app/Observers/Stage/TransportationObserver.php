<?php

namespace App\Observers\Stage;

use App\Contracts\StageInterface;
use App\Models\StatusMail;

class TransportationObserver
{
    public function created(StageInterface $status)
    {
        StatusMail::create([
            'custom_clearance_uid' => $status->custom_clearance_uid,
            'status_id'            => $status->id,
            'type'                 => 'transportation',
        ]);
        // CustomClearance::updateDate($status->custom_clearance_uid);
    }
    
    
    public function updated(StageInterface $status)
    {
        // CustomClearance::updateDate($status->custom_clearance_uid);
    }
}
