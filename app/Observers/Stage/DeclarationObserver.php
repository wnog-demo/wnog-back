<?php

namespace App\Observers\Stage;

use App\Models\{CustomClearance, Stage\Declaration as DeclarationStage, StatusMail};

class DeclarationObserver
{
    public function created(DeclarationStage $status)
    {
        StatusMail::create([
            'custom_clearance_uid' => $status->declaration->custom_clearance->uid,
            'status_id'            => $status->id,
            'type'                 => 'declaration',
        ]);
        
        // CustomClearance::updateDate($status->declaration->custom_clearance->uid);
    }
    
    
    public function updated(DeclarationStage $status)
    {
        // CustomClearance::updateDate($status->declaration->custom_clearance->uid);
    }
}
