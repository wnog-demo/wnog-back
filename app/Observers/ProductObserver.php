<?php

namespace App\Observers;

use Illuminate\Support\Str;
use App\Models\Product;

class ProductObserver
{
    public function creating(Product $product)
    {
        if (empty($product->en_name)) {
            $product->en_name = Str::ascii($product->name);
        }
    }
}
