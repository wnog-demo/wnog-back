<?php

namespace App\Observers;

use Illuminate\Support\Str;
use App\Models\Client;

class ClientObserver
{
    public function creating(Client $client)
    {
        if (empty($client->en_name)) {
            $client->en_name = Str::ascii($client->name);
        }
    }
}
