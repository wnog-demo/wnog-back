<?php

namespace App\Observers;

use Illuminate\Support\Str;
use App\Models\Custom;

class CustomObserver
{
    public function creating(Custom $custom)
    {
        if (empty($custom->en_fullname)) {
            $custom->en_fullname = Str::ascii($custom->fullname);
        }
    }
}
