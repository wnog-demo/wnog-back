<?php

namespace App\Observers;

use Illuminate\Support\Str;
use App\Models\ShipmentType;

class ShipmentTypeObserver
{
    public function creating(ShipmentType $type)
    {
        if (empty($type->en_name)) {
            $type->en_name = Str::ascii($type->name);
        }
    }
}
