<?php

namespace App\Models;

use Eloquent;
use Cviebrock\EloquentSluggable\Sluggable;

/**
 * Роли пользователей
 *
 * @property int    $id
 * @property string $name
 * @property string $slug
 *
 * @package App\Models
 */
class UserRole extends Eloquent
{
    use Sluggable;
    
    public $timestamps = false;
    
    protected $fillable = [
        'name',
        'slug',
    ];
    
    const ROLE_MANAGER     = 1;
    
    const ROLE_COORDINATOR = 2;
    
    
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name',
            ],
        ];
    }
}
