<?php

namespace App\Models;

use App\Jobs\SendStageEmail;
use App\Models\Stage\{Coming, Declaration, GreenLight, Transportation};
use Eloquent;
use Illuminate\Support\{Arr, Carbon};

/**
 * Статус таможенного оформления
 *
 * @property int    $id
 * @property string $custom_clearance_uid
 * @property int    $status_id
 * @property string $document_uid
 * @property string $type
 * @property Carbon $created_at
 *
 * @package App\Models
 */
class StatusMail extends Eloquent
{
    protected $fillable = [
        'custom_clearance_uid',
        'status_id',
        'document_uid',
        'type',
    ];
    
    protected $dates = [
        'created_at',
    ];
    
    
    public static function handle(): void
    {
        $data = [];
        
        self::latest()
            ->get()
            ->map(function (self $status) use (&$data) {
                $data[$status->custom_clearance_uid][] = $status;
            });
        
        if (empty($data)) {
            return;
        }
        
        foreach ($data as $uid => $items) {
            /** @var self $first */
            $first = Arr::first($items);
            $time = now()->getTimestamp() - 60;
            
            if ($first->created_at->getTimestamp() <= $time) {
                $statuses = [];
                
                /** @var self $item */
                foreach ($items as $item) {
                    switch ($item->type) {
                        case 'coming':
                            if ($stage = Coming::find($item->status_id)) {
                                $statuses[] = [
                                    'type'  => $item->type,
                                    'stage' => $stage,
                                ];
                            }
                            
                            break;
                        case 'transportation':
                            if ($stage = Transportation::find($item->status_id)) {
                                $statuses[] = [
                                    'type'  => $item->type,
                                    'stage' => $stage,
                                ];
                            }
                            
                            break;
                        case 'greenlight':
                            if ($stage = GreenLight::find($item->status_id)) {
                                $statuses[] = [
                                    'type'  => $item->type,
                                    'stage' => $stage,
                                ];
                            }
                            
                            break;
                        case 'declaration':
                            if ($stage = Declaration::find($item->status_id)) {
                                $statuses[] = [
                                    'type'  => $item->type,
                                    'stage' => $stage,
                                ];
                            }
                            
                            break;
                        case 'document':
                            if ($document = Document::where('uid', $item->document_uid)
                                ->limit(1)
                                ->first()) {
                                $statuses[] = [
                                    'type'  => $item->type,
                                    'stage' => $document,
                                ];
                            }
                            
                            break;
                    }
                    $item->delete();
                }
                
                $job = new SendStageEmail($statuses);
                $job->handle();
                // SendStageEmail::dispatch($statuses);
            }
        }
    }
}
