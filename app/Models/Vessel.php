<?php

namespace App\Models;

use Eloquent;

/**
 * Корабли
 *
 * @property string $uid
 * @property string $name
 *
 * @package App\Models
 */
class Vessel extends Eloquent
{
    protected $primaryKey = 'uid';
    
    public $incrementing = false;
    
    public $timestamps = false;
    
    protected $fillable = [
        'uid',
        'name',
    ];
}
