<?php

namespace App\Models;

use Eloquent;
use Illuminate\Support\Carbon;
use App\Contracts\StageInterface;
use App\Models\Stage\Declaration as DeclarationStage;

/**
 * Декларация
 *
 * @property string                $uid
 * @property string                $number
 * @property Carbon                $date
 *
 * @property-read DeclarationStage $stages
 * @property-read CustomClearance  $custom_clearance
 *
 * @package App\Models
 */
class Declaration extends Eloquent
{
    protected $primaryKey = 'uid';
    
    public $incrementing = false;
    
    public $timestamps = false;
    
    protected $fillable = [
        'uid',
        'custom_clearance_uid',
        'number',
        'date',
    ];
    
    protected $dates = [
        'date',
    ];
    
    
    public function stages()
    {
        return $this->hasMany(DeclarationStage::class);
    }
    
    
    public function custom_clearance()
    {
        return $this->belongsTo(CustomClearance::class);
    }
}
