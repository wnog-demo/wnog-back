<?php

namespace App\Models;

use Eloquent;
use App\Observers\ShipperObserver;

/**
 * Отправители-получатели
 *
 * @property string $uid
 * @property string $name
 * @property string $en_name
 * @property string $address
 *
 * @package App\Modelsp
 */
class Shipper extends Eloquent
{
    protected $primaryKey = 'uid';
    
    public $incrementing = false;
    
    public $timestamps = false;
    
    protected $fillable = [
        'uid',
        'name',
        'en_name',
        'address',
    ];
    
    
    protected static function boot()
    {
        parent::boot();
        parent::observe(ShipperObserver::class);
    }
}
