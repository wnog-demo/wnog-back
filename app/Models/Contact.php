<?php

namespace App\Models;

use Eloquent;

/**
 * Контактные лица вног
 *
 * @property string $uid
 * @property string $name
 * @property string $en_name
 *
 * @package App\Models
 */
class Contact extends Eloquent
{
    protected $primaryKey = 'uid';
    
    public $incrementing = false;
    
    public $timestamps = false;
    
    protected $fillable = [
        'uid',
        'name',
        'en_name',
    ];
}
