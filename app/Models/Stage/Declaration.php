<?php

namespace App\Models\Stage;

use App;
use App\Contracts\StageInterface;
use App\Models\Declaration as Dec;
use App\Observers\Stage\DeclarationObserver;
use Eloquent;
use Illuminate\Support\Carbon;

/**
 * Статус декларации
 *
 * @property string   $id
 * @property string   $name
 * @property string   $comment
 * @property string   $en_comment
 * @property Carbon   $date_status
 *
 * @property-read Dec $declaration
 *
 * @package App\Models
 */
class Declaration extends Eloquent implements StageInterface
{
    public $timestamps = false;
    
    const REGISTERED           = [
        'name'    => 'Зарегистрирована',
        'en_name' => 'Declaration registered',
    ];
    
    const ASSIGNED_INSPECTION  = [
        'name'    => 'Назначен досмотр',
        'en_name' => 'Customs inspection appointed',
    ];
    
    const INSPECTION_COMPLETED = [
        'name'    => 'Досмотр завершен',
        'en_name' => 'Customs inspection completed',
    ];
    
    const CONDITIONAL_RELEASE  = [
        'name'    => 'Условный выпуск',
        'en_name' => 'Conditional release',
    ];
    
    const RELEASED             = [
        'name'    => 'Выпущена',
        'en_name' => 'Customs clearance completed',
    ];
    
    const REFUSAL_TO_ISSUE     = [
        'name'    => 'Отказ в выпуске',
        'en_name' => 'Declaration declined',
    ];
    
    const PRE_DECLARATION      = [
        'name'    => 'Выпуск предварительной декларации',
        'en_name' => 'Pre-declaration release',
    ];
    
    protected $table = 'statuses_declaration';
    
    protected $fillable = [
        'declaration_uid',
        'name',
        'comment',
        'en_comment',
        'date_status',
    ];
    
    protected $dates = [
        'date_status',
    ];
    
    protected $hidden = [
        'declaration_uid',
    ];
    
    
    protected static function boot()
    {
        parent::boot();
        parent::observe(DeclarationObserver::class);
    }
    
    
    public function title(): string
    {
        $key = App::isLocale('en') ? 'en_name' : 'name';
        
        switch ($this->name) {
            case 'registered':
                return self::REGISTERED[$key];
            case 'assigned-inspection':
                return self::ASSIGNED_INSPECTION[$key];
            case 'inspection-completed':
                return self::INSPECTION_COMPLETED[$key];
            case 'conditional-release':
                return self::CONDITIONAL_RELEASE[$key];
            case 'released':
                return self::RELEASED[$key];
            case 'refusal-to-issue':
                return self::REFUSAL_TO_ISSUE[$key];
            case 'pre-declaration':
                return self::PRE_DECLARATION[$key];
            default:
                return '';
        }
    }
    
    
    public function status(): ?string
    {
        switch ($this->name) {
            case 'released':
                return self::STATUS_SUCCESS;
            case 'refusal-to-issue':
                return self::STATUS_FAIL;
            default:
                return self::STATUS_PROCESS;
        }
    }
    
    
    public function declaration()
    {
        return $this->belongsTo(Dec::class);
    }
}
