<?php

namespace App\Models\Stage;

use App;
use App\Contracts\StageInterface;
use App\Models\CustomClearance;
use App\Observers\Stage\GreenLightObserver;
use Eloquent;
use Illuminate\Support\Carbon;

/**
 * Статус Green Light
 *
 * @property string               $id
 * @property int                  $custom_clearance_uid
 * @property string               $name
 * @property string               $comment
 * @property string               $en_comment
 * @property Carbon               $date_status
 * @property Carbon               $date_confirmed
 *
 * @property-read CustomClearance $customClearance
 *
 * @package App\Models
 */
class GreenLight extends Eloquent implements StageInterface
{
    public $timestamps = false;
    
    const REQUEST_RECEIVED = [
        'name'    => 'Запрос получен',
        'en_name' => 'Request received',
    ];
    
    const CONFIRMED        = [
        'name'    => 'Подтвержден',
        'en_name' => 'Green Light confirmed',
    ];
    
    const NOT_CONFIRMED    = [
        'name'    => 'Не подтверждён',
        'en_name' => 'Green Light not confirmed',
    ];
    
    protected $table = 'statuses_green_light';
    
    protected $fillable = [
        'custom_clearance_uid',
        'name',
        'comment',
        'en_comment',
        'date_status',
        'date_confirmed',
    ];
    
    protected $dates = [
        'date_status',
        'date_confirmed',
    ];
    
    protected $hidden = [
        'custom_clearance_uid',
    ];
    
    
    protected static function boot()
    {
        parent::boot();
        parent::observe(GreenLightObserver::class);
    }
    
    
    public function title(): string
    {
        $key = App::isLocale('en') ? 'en_name' : 'name';
        
        switch ($this->name) {
            case 'request-received':
                return self::REQUEST_RECEIVED[$key];
            case 'confirmed':
                return trim(sprintf('%s %s', self::CONFIRMED[$key], $this->date_confirmed ? $this->date_confirmed->format('d.m.Y') : ''));
            case 'not-confirmed':
                return self::NOT_CONFIRMED[$key];
            default:
                return '';
        }
    }
    
    
    public function status(): ?string
    {
        switch ($this->name) {
            case 'confirmed':
                return self::STATUS_SUCCESS;
            case 'not-confirmed':
                return self::STATUS_FAIL;
            default:
                return self::STATUS_PROCESS;
        }
    }
    
    
    public function customClearance()
    {
        return $this->belongsTo(CustomClearance::class);
    }
}
