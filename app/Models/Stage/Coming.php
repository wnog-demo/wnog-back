<?php

namespace App\Models\Stage;

use App\Contracts\StageInterface;
use App\Models\CustomClearance;
use App\Observers\Stage\ComingObserver;
use Eloquent;
use Illuminate\Support\Carbon;

/**
 * Статус прибытия груза
 *
 * @property string               $id
 * @property int                  $custom_clearance_uid
 * @property string               $name
 * @property string               $comment
 * @property string               $en_comment
 * @property Carbon               $date
 * @property Carbon               $date_status
 *
 * @property-read CustomClearance $customClearance
 *
 * @package App\Models
 */
class Coming extends Eloquent implements StageInterface
{
    public $timestamps = false;
    
    protected $table = 'statuses_coming';
    
    protected $fillable = [
        'custom_clearance_uid',
        'name',
        'comment',
        'en_comment',
        'date',
        'date_status',
    ];
    
    protected $hidden = [
        'custom_clearance_uid',
    ];
    
    protected $dates = [
        'date',
        'date_status',
    ];
    
    
    protected static function boot()
    {
        parent::boot();
        parent::observe(ComingObserver::class);
    }
    
    
    public function title(): string
    {
        $title = $this->name == 'ata' ? 'ATA' : 'ETA';
        
        return trim(sprintf('%s %s', $title, $this->date ? $this->date->format('d.m.Y') : ''));
    }
    
    
    public function status(): ?string
    {
        switch ($this->name) {
            case 'ata':
                return self::STATUS_SUCCESS;
            case 'eta':
                return self::STATUS_PROCESS;
            default:
                return null;
        }
    }
    
    
    public function customClearance()
    {
        return $this->belongsTo(CustomClearance::class);
    }
}
