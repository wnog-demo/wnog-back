<?php

namespace App\Models\Stage;

use App;
use App\Contracts\StageInterface;
use App\Models\CustomClearance;
use App\Observers\Stage\TransportationObserver;
use Eloquent;
use Illuminate\Support\Carbon;

/**
 * Статус перевозки
 *
 * @property string               $id
 * @property int                  $custom_clearance_uid
 * @property string               $name
 * @property string               $comment
 * @property string               $en_comment
 * @property Carbon               $date_status
 *
 * @property-read CustomClearance $customClearance
 *
 * @package App\Models
 */
class Transportation extends Eloquent implements StageInterface
{
    public $timestamps = false;
    
    const WAITING_FOR_DECISION = [
        'name'    => 'Ожидаем решения',
        'en_name' => 'Pending decision',
    ];
    
    const CONFIRMED            = [
        'name'    => 'Подтверждена',
        'en_name' => 'Confirmed',
    ];
    
    const CARGO_SHIPPED        = [
        'name'    => 'Груз отправлен',
        'en_name' => 'Cargo dispatched',
    ];
    
    const NOT_CONFIRMED        = [
        'name'    => 'Не требуется',
        'en_name' => 'Not required',
    ];
    
    protected $table = 'statuses_transportation';
    
    protected $fillable = [
        'custom_clearance_uid',
        'name',
        'comment',
        'en_comment',
        'date_status',
    ];
    
    protected $dates = [
        'date_status',
    ];
    
    protected $hidden = [
        'custom_clearance_uid',
    ];
    
    
    protected static function boot()
    {
        parent::boot();
        parent::observe(TransportationObserver::class);
    }
    
    
    public function title(): string
    {
        $key = App::isLocale('en') ? 'en_name' : 'name';
        
        switch ($this->name) {
            case 'waiting':
                return self::WAITING_FOR_DECISION[$key];
            case 'confirmed':
                return self::CONFIRMED[$key];
            case 'not-confirmed':
                return self::NOT_CONFIRMED[$key];
            case 'cargo-shipped':
                return self::CARGO_SHIPPED[$key];
            default:
                return '';
        }
    }
    
    
    public function status(): ?string
    {
        return null;
    }
    
    
    public function customClearance()
    {
        return $this->belongsTo(CustomClearance::class);
    }
}
