<?php

namespace App\Models;

use App\Observers\ProductObserver;
use Eloquent;

/**
 * Продуктовая линейка
 *
 * @property string $uid
 * @property string $name
 * @property string $en_name
 *
 * @package App\Models
 */
class Product extends Eloquent
{
    protected $primaryKey = 'uid';
    
    public $incrementing = false;
    
    public $timestamps = false;
    
    protected $fillable = [
        'uid',
        'name',
        'en_name',
    ];
    
    
    protected static function boot()
    {
        parent::boot();
        parent::observe(ProductObserver::class);
    }
}
