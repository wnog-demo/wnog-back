<?php

namespace App\Models;

use Eloquent;

/**
 * Места отправки-доставки
 *
 * @property string $uid
 * @property string $title Заголовок (не элемент ORM!)
 * @property string $name
 * @property string $en_name
 * @property string $address
 * @property string $type
 *
 * @package App\Models
 */
class Place extends Eloquent
{
    protected $primaryKey = 'uid';
    
    public $incrementing = false;
    
    public $timestamps = false;
    
    protected $fillable = [
        'uid',
        'name',
        'en_name',
        'address',
    ];
}
