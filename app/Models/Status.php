<?php

namespace App\Models;

use Eloquent;

/**
 * Статус таможенного оформления
 *
 * @property string $uid
 * @property string $name
 * @property string $en_name
 * @property string $address
 *
 * @package App\Models
 */
class Status extends Eloquent
{
    protected $primaryKey = 'uid';
    
    public $incrementing = false;
    
    public $timestamps = false;
    
    protected $fillable = [
        'uid',
        'name',
        'en_name',
        'address',
    ];
}
