<?php

namespace App\Models;

use Eloquent;
use App\Observers\ShipmentTypeObserver;

/**
 * Наименование грузов
 *
 * @property string $uid
 * @property string $name
 * @property string $en_name
 * @property string $hazard_class
 * @property string $hazard_code
 * @property string $weight
 * @property string $quantity
 * @property mixed  pivot
 *
 * @package App\Models
 */
class ShipmentType extends Eloquent
{
    protected $primaryKey = 'uid';
    
    public $incrementing = false;
    
    public $timestamps = false;
    
    protected $fillable = [
        'uid',
        'name',
        'en_name',
        'hazard_class',
        'hazard_code',
    ];
    
    
    protected static function boot()
    {
        parent::boot();
        parent::observe(ShipmentTypeObserver::class);
    }
    
    
    public function getWeightAttribute(): string
    {
        return $this->pivot->weight;
    }
    
    
    public function getQuantityAttribute(): string
    {
        return $this->pivot->quantity;
    }
}
