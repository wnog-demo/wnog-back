<?php

namespace App\Models;

use App;
use App\Observers\DocumentObserver;
use Eloquent;
use Illuminate\Support\Carbon;
use Storage;

/**
 * Документ
 *
 * @property string               $uid
 * @property string               $custom_clearance_uid
 * @property string               $path
 * @property string               $name
 * @property string               $en_name
 * @property string               $category_name
 * @property string               $en_category_name
 * @property Carbon               $date
 *
 * @property-read CustomClearance $custom_clearance
 *
 * @package App\Models
 */
class Document extends Eloquent
{
    protected $primaryKey = 'uid';
    
    public $incrementing = false;
    
    protected $fillable = [
        'uid',
        'custom_clearance_uid',
        'path',
        'name',
        'en_name',
        'category_name',
        'en_category_name',
        'date',
    ];
    
    protected $dates = [
        'date',
    ];
    
    
    protected static function boot()
    {
        parent::boot();
        parent::observe(DocumentObserver::class);
    }
    
    
    public function getNameAttribute($value)
    {
        return App::isLocale('en') && ! empty($this->en_name) ? $this->en_name : $value;
    }
    
    
    public function getCategoryNameAttribute($value)
    {
        return App::isLocale('en') && ! empty($this->en_category_name) ? $this->en_category_name : $value;
    }
    
    
    public function custom_clearance()
    {
        return $this->belongsTo(CustomClearance::class);
    }
    
    
    public static function upload(string $document)
    {
        $ext = str_replace('@file/', '', substr($document, 5, strpos($document, ';') - 5));
        $filename = sprintf('%s.%s', md5(microtime() . mt_rand(0, 999)), $ext);
        $document = substr($document, strpos($document, ',') + 1);
        $document = base64_decode($document);
        $path = self::fullpath() . '/' . $filename;
        
        if (Storage::put($path, $document)) {
            return str_replace('public/', '/', $path);
        }
        
        return null;
    }
    
    
    protected static function fullpath()
    {
        $root = self::clearPath('public/documents');
        
        return sprintf('%s/%s', $root, Carbon::now()
            ->format('Y/m/d'));
    }
    
    
    protected static function clearPath(string $path): string
    {
        $path = array_diff(explode(DIRECTORY_SEPARATOR, $path), [null]);
        
        return implode(DIRECTORY_SEPARATOR, $path);
    }
}
