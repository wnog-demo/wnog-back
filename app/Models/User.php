<?php

namespace App\Models;

use App\Observers\UserObserver;
use App\Support\User as Authenticatable;
use Auth;
use Hash;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

/**
 * Клас пользователя
 *
 * @property string                          $uid
 * @property int                             $role_id
 * @property string                          $client_uid
 * @property bool                            $is_active
 * @property string                          $name
 * @property string                          $en_name
 * @property string                          $email
 * @property string                          $gender
 * @property string                          $password
 * @property string                          $password_hash
 *
 * @property-read  Client                    $client
 * @property-read  UserRole                  $role
 * @property-read  Collection|Notification[] $notifications
 *
 * @package App\Models
 */
class User extends Authenticatable
{
    use Notifiable;
    
    protected $primaryKey = 'uid';
    
    public $incrementing = false;
    
    protected $fillable = [
        'uid',
        'role_id',
        'client_uid',
        'is_active',
        'name',
        'en_name',
        'email',
        'gender',
    ];
    
    protected $hidden = [
        'password',
    ];
    
    
    public function setPasswordAttribute($password): void
    {
        if (empty($password)) {
            $this->attributes['password'] = '';
            
            return;
        }
        
        if (! (strlen($password) == 60 && preg_match('/^\$2y\$/', $password))) {
            $password = Hash::make($password);
        }
        
        $this->attributes['password'] = $password;
    }
    
    
    public function setEmailAttribute($value): void
    {
        $this->attributes['email'] = strtolower($value);
    }
    
    
    /*
    |--------------------------------------------------------------------------
    | Слушатели событий для модели
    |--------------------------------------------------------------------------
    */
    
    protected static function boot()
    {
        parent::boot();
        parent::observe(UserObserver::class);
    }
    
    
    /*
    |--------------------------------------------------------------------------
    | Роль пользователя
    |--------------------------------------------------------------------------
    */
    
    public function role()
    {
        return $this->belongsTo(UserRole::class);
    }
    
    
    /*
    |--------------------------------------------------------------------------
    | Клиент пользователя (для менеджеров)
    |--------------------------------------------------------------------------
    */
    
    public function client()
    {
        return $this->belongsTo(Client::class, 'client_uid', 'uid');
    }
    
    
    public function notifications()
    {
        return $this->belongsToMany(Notification::class, 'notification_user', 'user_uid', 'notification_id');
    }
    
    
    /*
    |--------------------------------------------------------------------------
    | Ссылка для установки пароля
    |--------------------------------------------------------------------------
    */
    
    public function passwordLink()
    {
        return sprintf('%s/set-password/?hash=%s&email=%s', env('APP_FRONT_URL'), $this->password_hash, $this->email);
    }
    
    
    /*
    |--------------------------------------------------------------------------
    | Ссылка для восстановление пароля
    |--------------------------------------------------------------------------
    */
    
    public function restorePasswordLink()
    {
        return sprintf('%s/restore-password/?hash=%s&email=%s', env('APP_FRONT_URL'), $this->password_hash, $this->email);
    }
    
    
    public static function check()
    {
        $user = Auth::user();
        
        return ! is_null($user) && $user->is_active;
    }
    
    
    public function generateHash()
    {
        return Hash::make($this->email . Str::random(60));
    }
}
