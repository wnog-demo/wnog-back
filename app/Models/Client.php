<?php

namespace App\Models;

use Eloquent;
use App\Observers\ClientObserver;

/**
 * Клиент
 *
 * @property string $uid
 * @property string $name
 * @property string $en_name
 *
 * @package App\Models
 */
class Client extends Eloquent
{
    protected $primaryKey = 'uid';
    
    public $incrementing = false;
    
    public $timestamps = false;
    
    protected $fillable = ['uid', 'name', 'en_name'];
    
    
    protected static function boot()
    {
        parent::boot();
        parent::observe(ClientObserver::class);
    }
}
