<?php

namespace App\Models;

use App;
use DB;
use Eloquent;
use Illuminate\Database\Query\JoinClause;

/**
 * Уведобления пользователя
 *
 * @property string $id
 * @property string $name
 * @property string $en_name
 * @property string $slug
 *
 * @package App\Models
 */
class Notification extends Eloquent
{
    public $timestamps = false;
    
    protected $fillable = [
        'uid',
        'name',
        'en_name',
        'slug',
    ];
    
    
    public function getNameAttribute($value)
    {
        return App::isLocale('en') && ! empty($this->en_name) ? $this->en_name : $value;
    }
    
    
    public static function initForUser(User $user): void
    {
        $notifications = Notification::where('is_active', true)
            ->get()
            ->map(function (\App\Models\Notification $notification) use ($user) {
                return [
                    'notification_id' => $notification->id,
                    'user_uid'        => $user->uid,
                ];
            })->toArray();
        DB::table('notification_user')->insert($notifications);
    }
    
    
    public static function valueByUser(string $userUID): ?array
    {
        $rows = DB::table('notification_user')
            ->select(['id', 'name', 'en_name', 'slug', 'value'])
            ->where('user_uid', $userUID)
            ->join('notifications', function (JoinClause $join) {
                $join->on('notifications.id', '=', 'notification_user.notification_id')
                    ->where('notifications.is_active', true);
            })
            ->orderBy('notifications.sort', 'asc')
            ->get();
        
        if (is_null($rows)) {
            return null;
        }
        
        return $rows->toArray();
    }
}
