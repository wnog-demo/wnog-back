<?php

namespace App\Models;

use Eloquent;

class CustomClearanceLog extends Eloquent
{
    public $timestamps = false;
    
    protected $fillable = [
        'user_uid',
        'action',
        'date',
    ];
    
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
