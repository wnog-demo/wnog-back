<?php

namespace App\Models;

use Eloquent;
use App\Observers\CustomObserver;

/**
 * Таможни
 *
 * @property string $uid
 * @property string $name
 * @property string $fullname
 * @property string $en_fullname
 * @property string $city
 * @property string $en_city
 *
 * @package App\Models
 */
class Custom extends Eloquent
{
    protected $primaryKey = 'uid';
    
    public $incrementing = false;
    
    public $timestamps = false;
    
    protected $fillable = [
        'uid',
        'name',
        'fullname',
        'en_fullname',
        'city',
        'en_city',
    ];
    
    
    protected static function boot()
    {
        parent::boot();
        parent::observe(CustomObserver::class);
    }
}
