<?php

namespace App\Models;

use App\Models\Stage\{Coming, GreenLight, Transportation};
use Eloquent;
use Illuminate\Support\{Carbon, Collection};
use Lang;

/**
 * Таможенное оформление
 *
 * @property string                           $uid                              UID Таможенного оформления
 * @property string                           $type                             Тип таможенного оформления (export|import)
 * @property Carbon                           $date                             Дата создания
 * @property string                           $reference                        Уникальный id груза
 * @property string                           $user_uid                         UID Пользователя
 * @property string                           $contact_uid                      UID контакта вног
 * @property string                           $customer_uid                     UID Клиента
 * @property string                           $custom_uid                       UID Таможни
 * @property string                           $vessel_uid                       UID Карабля
 * @property string                           $shipper_uid                      UID Отправителя
 * @property string                           $consignee_uid                    UID Получателя
 * @property string                           $product_uid                      UID Продуктовой линейки
 * @property string                           $port_of_discharge_uid            UID Порта выгрузки
 * @property string                           $port_of_loading_uid              UID Порта погрузки
 * @property string                           $place_of_receipt_uid             UID Места отправки
 * @property string                           $place_of_delivery_uid            UID Места доставки
 * @property string                           $bol                              Накладная (корабль)
 * @property string                           $awb                              Накладная (авиа)
 * @property string                           $rwb                              Накладная (поезд)
 * @property string                           $cmr                              Накладная (авто)
 *
 * @property User                             $user                             Менеджер/Координатор
 * @property Contact                          $contact                          Контакт ВНОГ
 * @property Client                           $customer                         Клиент
 * @property Custom                           $custom                           Таможня
 * @property Vessel                           $vessel                           Карабль
 * @property Shipper                          $shipper                          Отправитель
 * @property Shipper                          $consignee                        Получатель
 * @property Product                          $product                          Продуктовая линейка
 * @property Place                            $port_of_discharge                Порт выгрузки
 * @property Place                            $port_of_loading                  Порт погрузки
 * @property Place                            $place_of_receipt                 Место отправки
 * @property Place                            $place_of_delivery                Место доставки
 * @property ShipmentType[]                   $shipments                        Грузы
 *
 * @property-read Collection|Document[]       $documents                        Документы
 * @property-read Collection|GreenLight[]     $greenlights                      Статусы Green Light
 * @property-read Collection|Coming[]         $comings                          Статусы прибытия груза
 * @property-read Collection|Transportation[] $transportations                  Статусы перевозки
 * @property-read Collection|Declaration[]    $declarations                     Декларации
 *
 *
 * @package App\Models
 */
class CustomClearance extends Eloquent
{
    protected $primaryKey = 'uid';
    
    public $incrementing = false;
    
    protected $fillable = [
        'uid',
        'type',
        'date',
        'reference',
        'user_uid',
        'contact_uid',
        'customer_uid',
        'custom_uid',
        'vessel_uid',
        'shipper_uid',
        'consignee_uid',
        'product_uid',
        'port_of_discharge_uid',
        'port_of_loading_uid',
        'place_of_receipt_uid',
        'place_of_delivery_uid',
        'bol',
        'awb',
        'rwb',
        'cmr',
    ];
    
    protected $hidden = [
        'created_at',
        'updated_at',
    ];
    
    protected $dates = [
        'created_at',
        'updated_at',
        'date',
    ];
    
    public static $load = [
        'user',
        'contact',
        'customer',
        'custom',
        'vessel',
        'shipper',
        'consignee',
        'product',
        'port_of_discharge',
        'port_of_loading',
        'place_of_receipt',
        'place_of_delivery',
        'shipments',
        'greenlights',
        'comings',
        'documents',
        'declarations',
        'declarations.stages',
        'transportations',
    ];
    
    
    public function user()
    {
        return $this->belongsTo(User::class, 'user_uid', 'uid');
    }
    
    
    public function contact()
    {
        return $this->belongsTo(Contact::class, 'contact_uid', 'uid');
    }
    
    
    public function customer()
    {
        return $this->belongsTo(Client::class, 'customer_uid', 'uid');
    }
    
    
    public function custom()
    {
        return $this->belongsTo(Custom::class, 'custom_uid', 'uid');
    }
    
    
    public function vessel()
    {
        return $this->belongsTo(Vessel::class, 'vessel_uid', 'uid');
    }
    
    
    public function shipper()
    {
        return $this->belongsTo(Shipper::class, 'shipper_uid', 'uid');
    }
    
    
    public function consignee()
    {
        return $this->belongsTo(Shipper::class, 'consignee_uid', 'uid');
    }
    
    
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_uid', 'uid');
    }
    
    
    public function port_of_discharge()
    {
        return $this->belongsTo(Place::class, 'port_of_discharge_uid', 'uid');
    }
    
    
    public function port_of_loading()
    {
        return $this->belongsTo(Place::class, 'port_of_loading_uid', 'uid');
    }
    
    
    public function place_of_receipt()
    {
        return $this->belongsTo(Place::class, 'place_of_receipt_uid', 'uid');
    }
    
    
    public function place_of_delivery()
    {
        return $this->belongsTo(Place::class, 'place_of_delivery_uid', 'uid');
    }
    
    
    public function greenlights()
    {
        return $this->hasMany(GreenLight::class, 'custom_clearance_uid', 'uid');
    }
    
    
    public function comings()
    {
        return $this->hasMany(Coming::class, 'custom_clearance_uid', 'uid')
                    ->orderBy('id');
    }
    
    
    public function documents()
    {
        return $this->hasMany(Document::class, 'custom_clearance_uid', 'uid')
                    ->orderBy('date');
    }
    
    
    public function declarations()
    {
        return $this->hasMany(Declaration::class, 'custom_clearance_uid', 'uid')
                    ->orderBy('id');
    }
    
    
    public function transportations()
    {
        return $this->hasMany(Transportation::class, 'custom_clearance_uid', 'uid');
    }
    
    
    public function shipments()
    {
        return $this->BelongsToMany(ShipmentType::class)
                    ->withPivot(['quantity', 'weight']);
    }
    
    
    public function route()
    {
        $results = [];
        
        if (! empty($this->awb)) {
            $results = [
                'type'   => 'awb',
                'number' => $this->awb,
                'titles' => [
                    'place_of_receipt'  => Lang::trans('places.awb-place-of-receipt'),
                    'port_of_loading'   => Lang::trans('places.awb-port-of-loading'),
                    'port_of_discharge' => Lang::trans('places.awb-port-of-discharge'),
                    'place_of_delivery' => Lang::trans('places.awb-place-of-delivery'),
                ],
            ];
        } elseif (! empty($this->bol)) {
            $results = [
                'type'   => 'b/l',
                'number' => $this->bol,
                'titles' => [
                    'place_of_receipt'  => Lang::trans('places.bol-place-of-receipt'),
                    'port_of_loading'   => Lang::trans('places.bol-port-of-loading'),
                    'port_of_discharge' => Lang::trans('places.bol-port-of-discharge'),
                    'place_of_delivery' => Lang::trans('places.bol-place-of-delivery'),
                ],
            ];
        } elseif (! empty($this->rwb)) {
            $results = [
                'type'   => 'rwb',
                'number' => $this->rwb,
                'titles' => [
                    'place_of_receipt'  => Lang::trans('places.rwb-place-of-receipt'),
                    'port_of_loading'   => Lang::trans('places.rwb-port-of-loading'),
                    'port_of_discharge' => Lang::trans('places.rwb-port-of-discharge'),
                    'place_of_delivery' => Lang::trans('places.rwb-place-of-delivery'),
                ],
            ];
        } elseif (! empty($this->cmr)) {
            $results = [
                'type'   => 'cmr',
                'number' => $this->cmr,
                'titles' => [
                    'place_of_receipt'  => Lang::trans('places.cmr-place-of-receipt'),
                    'port_of_loading'   => Lang::trans('places.cmr-port-of-loading'),
                    'port_of_discharge' => Lang::trans('places.cmr-port-of-discharge'),
                    'place_of_delivery' => Lang::trans('places.cmr-place-of-delivery'),
                ],
            ];
        }
        
        if (! empty($results)) {
            $uids = [];
            
            if (! is_null($this->port_of_loading)) {
                $uids[] = $this->port_of_loading->uid;
            }
            
            if (! is_null($this->port_of_discharge)) {
                $uids[] = $this->port_of_discharge->uid;
            }
            
            if (! is_null($this->place_of_receipt) && ! in_array($this->place_of_receipt->uid, $uids)) {
                $this->place_of_receipt->title = $results['titles']['place_of_receipt'];
                $this->place_of_receipt->type = 'place_of_receipt';
                $results['list'][] = $this->place_of_receipt;
            }
            
            if (! is_null($this->port_of_loading)) {
                $this->port_of_loading->title = $results['titles']['port_of_loading'];
                $this->port_of_loading->type = 'port_of_loading';
                $results['list'][] = $this->port_of_loading;
            }
            
            if (! is_null($this->port_of_discharge)) {
                $this->port_of_discharge->title = $results['titles']['port_of_discharge'];
                $this->port_of_discharge->type = 'port_of_discharge';
                $results['list'][] = $this->port_of_discharge;
            }
            
            if (! is_null($this->place_of_delivery) && ! in_array($this->place_of_delivery->uid, $uids)) {
                $this->place_of_delivery->title = $results['titles']['place_of_delivery'];
                $this->place_of_delivery->type = 'place_of_delivery';
                $results['list'][] = $this->place_of_delivery;
            }
            
            unset($results['titles']);
            
            return $results;
        }
        
        return null;
    }
    
    
    public static function updateDate(string $uid)
    {
        $clearance = CustomClearance::where('uid', $uid)
                                    ->limit(1)
                                    ->first();
        \DB::table('custom_clearances')
           ->where('uid', $clearance->uid)
           ->update([
               'date'       => $clearance->date,
               'updated_at' => now(),
           ]);
    }
}
