<?php

namespace App\Contracts;

use App\Models\CustomClearance;
use Illuminate\Support\Carbon;

/**
 * Interface StageInterface
 *
 * @property string          $id
 * @property int             $custom_clearance_uid
 * @property string          $name
 * @property string          $comment
 * @property string          $en_comment
 * @property Carbon          $date
 * @property Carbon          $date_status
 *
 * @property CustomClearance $customClearance
 *
 * @package App\Contracts
 */
interface StageInterface
{
    const STATUS_FAIL    = 'failure';
    
    const STATUS_PROCESS = 'process';
    
    const STATUS_SUCCESS = 'success';
    
    
    public function title(): string;
    
    
    public function status(): ?string;
}
