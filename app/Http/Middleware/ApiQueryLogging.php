<?php

namespace App\Http\Middleware;

use Storage;
use File;
use Closure;
use Illuminate\Http\Request;

class ApiQueryLogging
{
    public function handle(Request $request, Closure $next)
    {
        $method = '';
        
        switch ($request->method()) {
            case 'POST':
                $method = 'create';
                break;
            case 'PUT':
                $method = 'update';
                break;
            case 'DELETE':
                $method = 'delete';
                break;
        }
        if (! empty($method)) {
            $uris = array_diff(explode('/', $request->getRequestUri()), [null, 'api']);
            $path = sprintf('logs/api/%s_%s_%s.json', $method, implode('_', $uris), time());
            Storage::disk('public')->put($path, json_encode($request->all(), JSON_UNESCAPED_UNICODE));
        }
        
        return $next($request);
    }
}
