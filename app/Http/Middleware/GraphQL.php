<?php

namespace App\Http\Middleware;

use App;
use Closure;
use Request;

class GraphQL
{
    public function handle($request, Closure $next, $guard = null)
    {
        App::setLocale(Request::header('X-Lang') ?: 'ru');
        config(['auth.defaults.guard' => $guard]);
        
        return $next($request);
    }
}
