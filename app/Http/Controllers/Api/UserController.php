<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserRole;
use App\Notifications\UpdateEmail;
use Illuminate\Http\{JsonResponse, Request};
use Validator;

class UserController extends Controller
{
    protected static $rules = [
        'name'       => 'required|string|max:255',
        'email'      => 'required|string|max:255|unique:users',
        'is_head'    => 'nullable|boolean',
        'client_uid' => 'nullable|string|max:255|exists:clients,uid',
        'en_name'    => 'nullable|string|max:255',
        'gender'     => 'nullable|string|in:male,female',
    ];
    
    
    public function store(Request $request): JsonResponse
    {
        $rules = array_merge(['uid' => 'required|string|max:255|unique:users'], self::$rules);
        $fields = $request->only(array_keys($rules));
        $validator = Validator::make($fields, $rules);
        
        if ($validator->fails()) {
            return parent::error($validator->errors()->first());
        }
        
        $fields['is_head'] = $fields['is_head'] ?? false;
        
        if ($fields['is_head']) {
            $fields['role_id'] = UserRole::ROLE_MANAGER;
        } else {
            $fields['role_id'] = UserRole::ROLE_COORDINATOR;
        }
        
        $user = User::create($fields);
        
        return parent::success($user->fresh()->toArray(), 201);
    }
    
    
    public function update(Request $request, $uid): JsonResponse
    {
        $rules = array_merge(['is_active' => 'nullable|boolean'], self::$rules);
        $rules['name'] = 'nullable|string|max:255';
        unset($rules['email']);
        $fields = $request->only(array_keys($rules));
        $validator = Validator::make($fields, $rules);
        
        if ($validator->fails()) {
            return parent::error($validator->errors()->first());
        }
        
        $user = User::where('uid', $uid)
            ->limit(1)
            ->first();
        
        if (is_null($user)) {
            return parent::error('User not found.');
        }
        
        if (isset($fields['is_head'])) {
            $fields['role_id'] = $fields['is_head'] ? UserRole::ROLE_MANAGER : UserRole::ROLE_COORDINATOR;
        }
        
        $user->update($fields);
        
        return parent::success($user->fresh()->toArray());
    }
    
    
    public function destroy($uid): JsonResponse
    {
        $user = User::where('uid', $uid)
            ->limit(1)
            ->first();
        
        if (is_null($user)) {
            return parent::error('User not found.');
        }
        
        $user->is_active = false;
        $user->save();
        
        return parent::success([], 204);
    }
    
    
    public function updateEmail(Request $request, $uid): JsonResponse
    {
        $fields = $request->only('email');
        $validator = Validator::make($fields, ['email' => 'required|string|max:255|unique:users']);
        
        if ($validator->fails()) {
            return parent::error($validator->errors()->first());
        }
        
        $user = User::where('uid', $uid)
            ->limit(1)
            ->first();
        
        if (is_null($user)) {
            return parent::error('User not found.');
        }
        
        $user->is_active = false;
        $user->email = $fields['email'];
        $user->password = null;
        $user->password_hash = $user->generateHash();
        $user->notify(new UpdateEmail());
        $user->save();
        
        return parent::success($user->toArray());
    }
}
