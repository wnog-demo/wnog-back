<?php

namespace App\Http\Controllers\Api;

use Exception;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\{Request, JsonResponse};
use App\Models\{CustomClearance, Product};

class ProductController extends Controller
{
    protected static $rules = [
        'name'    => 'required|string|max:255',
        'en_name' => 'nullable|string|max:255',
    ];
    
    
    public function store(Request $request): JsonResponse
    {
        $rules = array_merge(['uid' => 'required|string|unique:products'], self::$rules);
        $fields = $request->only(array_keys($rules));
        $validator = Validator::make($fields, $rules);
        
        if ($validator->fails()) {
            return parent::error($validator->errors()->first());
        }
        
        $product = Product::create($fields);
        
        return parent::success($product->fresh()->toArray(), 201);
    }
    
    
    public function update(Request $request, $uid): JsonResponse
    {
        $fields = $request->only(array_keys(self::$rules));
        $validator = Validator::make($fields, self::$rules);
        
        if ($validator->fails()) {
            return parent::error($validator->errors()->first());
        }
        
        $product = Product::where('uid', $uid)
            ->limit(1)
            ->first();
        
        if (is_null($product)) {
            return parent::error('Product not found.');
        }
        
        $product->update($fields);
        
        return parent::success($product->fresh()->toArray());
    }
    
    
    public function destroy($uid): JsonResponse
    {
        $product = Product::where('uid', $uid)
            ->limit(1)
            ->first();
        
        if (is_null($product)) {
            return parent::error('Product not found.');
        }
        
        CustomClearance::where('product_uid', $uid)
            ->update(['product_uid' => null]);
        
        try {
            $product->delete();
        } catch (Exception $exception) {
            return parent::error($exception->getMessage());
        }
        
        return parent::success([], 204);
    }
}
