<?php

namespace App\Http\Controllers\Api;

use Exception;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\{Request, JsonResponse};
use App\Models\{CustomClearance, Place};

class PlaceController extends Controller
{
    protected static $rules = [
        'name'    => 'required|string|max:255',
        'en_name' => 'nullable|string|max:255',
        'address' => 'nullable|string|max:255',
    ];
    
    
    public function store(Request $request): JsonResponse
    {
        $rules = array_merge(['uid' => 'required|string|unique:places'], self::$rules);
        $fields = $request->only(array_keys($rules));
        $validator = Validator::make($fields, $rules);
        
        if ($validator->fails()) {
            return parent::error($validator->errors()->first());
        }
        
        $place = Place::create($fields);
        
        return parent::success($place->fresh()->toArray(), 201);
    }
    
    
    public function update(Request $request, $uid): JsonResponse
    {
        $fields = $request->only(array_keys(self::$rules));
        $validator = Validator::make($fields, self::$rules);
        
        if ($validator->fails()) {
            return parent::error($validator->errors()->first());
        }
        
        $place = Place::where('uid', $uid)
            ->limit(1)
            ->first();
        
        if (is_null($place)) {
            return parent::error('Place not found.');
        }
        
        $place->update($fields);
        
        return parent::success($place->fresh()->toArray());
    }
    
    
    public function destroy($uid): JsonResponse
    {
        $place = Place::where('uid', $uid)
            ->limit(1)
            ->first();
        
        if (is_null($place)) {
            return parent::error('Place not found.');
        }
        
        CustomClearance::where('port_of_discharge_uid', $uid)
            ->update(['port_of_discharge_uid' => null]);
        
        CustomClearance::where('port_of_loading_uid', $uid)
            ->update(['port_of_loading_uid' => null]);
        
        CustomClearance::where('place_of_receipt_uid', $uid)
            ->update(['place_of_receipt_uid' => null]);
        
        CustomClearance::where('place_of_delivery_uid', $uid)
            ->update(['place_of_delivery_uid' => null]);
        
        try {
            $place->delete();
        } catch (Exception $exception) {
            return parent::error($exception->getMessage());
        }
        
        return parent::success([], 204);
    }
}
