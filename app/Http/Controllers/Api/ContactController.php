<?php

namespace App\Http\Controllers\Api;

use Exception;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\{Request, JsonResponse};
use App\Models\{Contact, CustomClearance};

class ContactController extends Controller
{
    protected static $rules = [
        'name'    => 'required|string|max:255',
        'en_name' => 'nullable|string|max:255',
    ];
    
    
    public function store(Request $request): JsonResponse
    {
        $rules = array_merge(['uid' => 'required|string|unique:contacts'], self::$rules);
        $fields = $request->only(array_keys($rules));
        $validator = Validator::make($fields, $rules);
        
        if ($validator->fails()) {
            return parent::error($validator->errors()->first());
        }
        
        $contact = Contact::create($fields);
        
        return parent::success($contact->fresh()->toArray(), 201);
    }
    
    
    public function update(Request $request, $uid): JsonResponse
    {
        $fields = $request->only(array_keys(self::$rules));
        $validator = Validator::make($fields, self::$rules);
        
        if ($validator->fails()) {
            return parent::error($validator->errors()->first());
        }
        
        $contact = Contact::where('uid', $uid)
            ->limit(1)
            ->first();
        
        if (is_null($contact)) {
            return parent::error('Contact not found.');
        }
        
        $contact->update($fields);
        
        return parent::success($contact->fresh()->toArray());
    }
    
    
    public function destroy($uid): JsonResponse
    {
        $contact = Contact::where('uid', $uid)
            ->limit(1)
            ->first();
        
        if (is_null($contact)) {
            return parent::error('Contact not found.');
        }
        
        CustomClearance::where('contact_uid', $uid)
            ->update(['contact_uid' => null]);
        
        try {
            $contact->delete();
        } catch (Exception $exception) {
            return parent::error($exception->getMessage());
        }
        
        return parent::success([], 204);
    }
}
