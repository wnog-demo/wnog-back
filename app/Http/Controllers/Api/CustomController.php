<?php

namespace App\Http\Controllers\Api;

use Exception;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\{Request, JsonResponse};
use App\Models\{CustomClearance, Custom};

class CustomController extends Controller
{
    protected static $rules = [
        'name'        => 'required|string|max:255',
        'fullname'    => 'nullable|string|max:255',
        'en_fullname' => 'nullable|string|max:255',
        'city'        => 'nullable|string|max:255',
        'en_city'     => 'nullable|string|max:255',
    ];
    
    
    public function store(Request $request): JsonResponse
    {
        $rules = array_merge(['uid' => 'required|string|unique:customs'], self::$rules);
        $fields = $request->only(array_keys($rules));
        $validator = Validator::make($fields, $rules);
        
        if ($validator->fails()) {
            return parent::error($validator->errors()->first());
        }
        
        $fields = [
            'uid'         => $fields['uid'],
            'name'        => $fields['en_fullname'],
            'fullname'    => $fields['fullname'],
            'en_fullname' => $fields['name'],
            'city'        => $fields['city'],
            'en_city'     => $fields['en_city'],
        ];
        
        $custom = Custom::create($fields);
        
        return parent::success($custom->fresh()->toArray(), 201);
    }
    
    
    public function update(Request $request, $uid): JsonResponse
    {
        $fields = $request->only(array_keys(self::$rules));
        $validator = Validator::make($fields, self::$rules);
        
        if ($validator->fails()) {
            return parent::error($validator->errors()->first());
        }
        
        $custom = Custom::where('uid', $uid)
            ->limit(1)
            ->first();
        
        if (is_null($custom)) {
            return parent::error('Custom not found.');
        }
        
        $custom->update($fields);
        
        return parent::success($custom->fresh()->toArray());
    }
    
    
    public function destroy($uid): JsonResponse
    {
        $custom = Custom::where('uid', $uid)
            ->limit(1)
            ->first();
        
        if (is_null($custom)) {
            return parent::error('Custom not found.');
        }
        
        CustomClearance::where('custom_uid', $uid)
            ->update(['custom_uid' => null]);
        
        try {
            $custom->delete();
        } catch (Exception $exception) {
            return parent::error($exception->getMessage());
        }
        
        return parent::success([], 204);
    }
}
