<?php

namespace App\Http\Controllers\Api;

use DB;
use Exception;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\{Request, JsonResponse};
use App\Models\{CustomClearance, Declaration};
use App\Models\Stage\{GreenLight, Coming, Declaration as DeclarationStage, Transportation};

class CustomClearanceController extends Controller
{
    protected static $rules = [
        'type'                  => 'required|string|in:export,import',
        'date'                  => 'required|date_format:"Y-m-d H:i:s"',
        'reference'             => 'required|string|max:255',
        'user_uid'              => 'nullable|string|max:255|exists:users,uid',
        'contact_uid'           => 'nullable|string|max:255|exists:contacts,uid',
        'customer_uid'          => 'nullable|string|max:255|exists:clients,uid',
        'custom_uid'            => 'nullable|string|max:255|exists:customs,uid',
        'vessel_uid'            => 'nullable|string|max:255|exists:vessels,uid',
        'shipper_uid'           => 'nullable|string|max:255|exists:shippers,uid',
        'consignee_uid'         => 'nullable|string|max:255|exists:shippers,uid',
        'product_uid'           => 'nullable|string|max:255|exists:products,uid',
        'port_of_discharge_uid' => 'nullable|string|max:255|exists:places,uid',
        'port_of_loading_uid'   => 'nullable|string|max:255|exists:places,uid',
        'place_of_receipt_uid'  => 'nullable|string|max:255|exists:places,uid',
        'place_of_delivery_uid' => 'nullable|string|max:255|exists:places,uid',
        'bol'                   => 'nullable|string|max:255',
        'awb'                   => 'nullable|string|max:255',
        'rwb'                   => 'nullable|string|max:255',
        'cmr'                   => 'nullable|string|max:255',
        
        'status'                       => 'nullable|array',
        'status.greenlight'            => 'nullable|array',
        'status.greenlight.name'       => 'string|max:255|in:request-received,confirmed,not-confirmed',
        'status.greenlight.comment'    => 'nullable|string|max:255',
        'status.greenlight.en_comment' => 'nullable|string|max:255',
        'status.greenlight.date'       => 'nullable|date_format:"Y-m-d H:i:s"',
        
        'status.coming.name'       => 'string|max:255|in:eta,ata',
        'status.coming.comment'    => 'nullable|string|max:255',
        'status.coming.en_comment' => 'nullable|string|max:255',
        'status.coming.date'       => 'nullable|date_format:"Y-m-d H:i:s"',
        
        'status.transportation.name'       => 'string|max:255|in:waiting,confirmed,not-confirmed,cargo-shipped',
        'status.transportation.comment'    => 'nullable|string|max:255',
        'status.transportation.en_comment' => 'nullable|string|max:255',
        
        'status.declarations.*.uid'        => 'string|max:255',
        'status.declarations.*.number'     => 'nullable|string|max:255',
        'status.declarations.*.name'       => 'string|max:255|in:registered,assigned-inspection,inspection-completed,conditional-release,released,refusal-to-issue,pre-declaration',
        'status.declarations.*.comment'    => 'nullable|string|max:255',
        'status.declarations.*.en_comment' => 'nullable|string|max:255',
        'status.declarations.*.date'       => 'nullable|date_format:"Y-m-d H:i:s"',
        
        'shipments'                     => 'nullable|array',
        'shipments.*.shipment_type_uid' => 'exists:shipment_types,uid',
        'shipments.*.quantity'          => 'nullable|string|max:255',
        'shipments.*.weight'            => 'nullable|string|max:255',
    ];
    
    
    public function store(Request $request): JsonResponse
    {
        $rules = array_merge(['uid' => 'required|string|unique:custom_clearances'], self::$rules);
        $fields = $request->only(array_keys($rules));
        $validator = Validator::make($fields, $rules);
        
        if ($validator->fails()) {
            return parent::error($validator->errors()->first());
        }
        
        $customClearance = CustomClearance::create($fields);
        
        if (! empty($fields['status'])) {
            $this->status($customClearance->uid, $fields['status']);
        }
        
        if (isset($fields['shipments'])) {
            $this->shipments($customClearance->uid, $fields['shipments']);
        }
        
        return parent::success($customClearance->fresh()->load(CustomClearance::$load)->toArray(), 201);
    }
    
    
    public function update(Request $request, $uid): JsonResponse
    {
        $fields = $request->only(array_keys(self::$rules));
        $validator = Validator::make($fields, self::$rules);
        
        if ($validator->fails()) {
            return parent::error($validator->errors()->first());
        }
        
        $customClearance = CustomClearance::where('uid', $uid)
            ->limit(1)
            ->first();
        
        if (is_null($customClearance)) {
            return parent::error('Custom clearance not found.');
        }
        
        $customClearance->update($fields);
        
        return parent::success($customClearance->fresh()->load(CustomClearance::$load)->toArray());
    }
    
    
    public function destroy($uid): JsonResponse
    {
        $customClearance = CustomClearance::where('uid', $uid)
            ->limit(1)
            ->first();
        
        if (is_null($customClearance)) {
            return parent::error('Custom clearance not found.');
        }
        
        try {
            $customClearance->delete();
        } catch (Exception $exception) {
            return parent::error($exception->getMessage());
        }
        
        return parent::success([], 204);
    }
    
    
    protected function status(string $uid, array $data): void
    {
        if (isset($data['declarations'])) {
            unset($data['declarations']['*']);
            $this->declaration($uid, $data['declarations']);
        }
        
        if (isset($data['coming'])) {
            $this->coming($uid, $data['coming']);
        }
        
        if (isset($data['greenlight'])) {
            $this->greenlight($uid, $data['greenlight']);
        }
        
        if (isset($data['transportation'])) {
            $this->transportation($uid, $data['transportation']);
        }
    }
    
    
    protected function declaration(string $uid, array $data): void
    {
        foreach ($data as $resource) {
            $declaration = Declaration::create(array_merge($resource, ['custom_clearance_uid' => $uid]));
            DeclarationStage::create(array_merge($resource, ['declaration_uid' => $declaration->uid]));
        }
    }
    
    
    protected function coming(string $uid, array $data): void
    {
        Coming::create(array_merge($data, ['custom_clearance_uid' => $uid]));
    }
    
    
    protected function greenlight(string $uid, array $data): void
    {
        GreenLight::create(array_merge($data, ['custom_clearance_uid' => $uid]));
    }
    
    
    protected function transportation(string $uid, array $data): void
    {
        Transportation::create(array_merge($data, ['custom_clearance_uid' => $uid]));
    }
    
    
    protected function shipments(string $uid, array $data): void
    {
        unset($data['*']);
        
        foreach ($data as $shipment) {
            DB::table('custom_clearance_shipment_type')
                ->insert(array_merge($shipment, ['custom_clearance_uid' => $uid]));
        }
    }
}
