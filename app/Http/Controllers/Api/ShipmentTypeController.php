<?php

namespace App\Http\Controllers\Api;

use Exception;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\{Request, JsonResponse};
use App\Models\ShipmentType;

class ShipmentTypeController extends Controller
{
    protected static $rules = [
        'name'         => 'required|string|max:255',
        'en_name'      => 'nullable|string|max:255',
        'hazard_class' => 'nullable|string|max:255',
        'hazard_code'  => 'nullable|string|max:255',
    ];
    
    
    public function store(Request $request): JsonResponse
    {
        $rules = array_merge(['uid' => 'required|string|unique:shipment_types'], self::$rules);
        $fields = $request->only(array_keys($rules));
        $validator = Validator::make($fields, $rules);
        
        if ($validator->fails()) {
            return parent::error($validator->errors()->first());
        }
        
        $type = ShipmentType::create($fields);
        
        return parent::success($type->fresh()->toArray(), 201);
    }
    
    
    public function update(Request $request, $uid): JsonResponse
    {
        $fields = $request->only(array_keys(self::$rules));
        $validator = Validator::make($fields, self::$rules);
        
        if ($validator->fails()) {
            return parent::error($validator->errors()->first());
        }
        
        $type = ShipmentType::where('uid', $uid)
            ->limit(1)
            ->first();
        
        if (is_null($type)) {
            return parent::error('Shipment type not found.');
        }
        
        $type->update($fields);
        
        return parent::success($type->fresh()->toArray());
    }
    
    
    public function destroy($uid): JsonResponse
    {
        $type = ShipmentType::where('uid', $uid)
            ->limit(1)
            ->first();
        
        if (is_null($type)) {
            return parent::error('Shipment type not found.');
        }
        
        try {
            $type->delete();
        } catch (Exception $exception) {
            return parent::error($exception->getMessage());
        }
        
        return parent::success([], 204);
    }
}
