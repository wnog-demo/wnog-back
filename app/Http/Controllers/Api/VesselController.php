<?php

namespace App\Http\Controllers\Api;

use Exception;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\{Request, JsonResponse};
use App\Models\{CustomClearance, Vessel};

class VesselController extends Controller
{
    public function store(Request $request): JsonResponse
    {
        $fields = $request->all();
        $validator = Validator::make($fields, [
            'uid'  => 'required|string|unique:vessels',
            'name' => 'required|string|max:255',
        ]);
        
        if ($validator->fails()) {
            return parent::error($validator->errors()->first());
        }
        
        $vessel = Vessel::create($fields);
        
        return parent::success($vessel->fresh()->toArray(), 201);
    }
    
    
    public function update(Request $request, $uid): JsonResponse
    {
        $fields = $request->all();
        $validator = Validator::make($fields, [
            'name' => 'required|string|max:255',
        ]);
        
        if ($validator->fails()) {
            return parent::error($validator->errors()->first());
        }
        
        $vessel = Vessel::where('uid', $uid)
            ->limit(1)
            ->first();
        
        if (is_null($vessel)) {
            return parent::error('Vessel not found.');
        }
        
        $vessel->update($fields);
        
        return parent::success($vessel->fresh()->toArray());
    }
    
    
    public function destroy($uid): JsonResponse
    {
        $vessel = Vessel::where('uid', $uid)
            ->limit(1)
            ->first();
        
        if (is_null($vessel)) {
            return parent::error('Vessel not found.');
        }
        
        CustomClearance::where('vessel_uid', $vessel->uid)
            ->update(['vessel_uid' => null]);
        
        try {
            $vessel->delete();
        } catch (Exception $exception) {
            return parent::error($exception->getMessage());
        }
        
        return parent::success([], 204);
    }
}
