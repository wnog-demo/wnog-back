<?php

namespace App\Http\Controllers\Api;

use Exception;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\{Request, JsonResponse};
use App\Models\{CustomClearance, User, Client};

class ClientController extends Controller
{
    protected static $rules = [
        'name'    => 'required|string|max:255',
        'en_name' => 'nullable|string|max:255',
    ];
    
    
    public function store(Request $request): JsonResponse
    {
        $rules = array_merge(['uid' => 'required|string|unique:clients'], self::$rules);
        $fields = $request->only(array_keys($rules));
        $validator = Validator::make($fields, $rules);
        
        if ($validator->fails()) {
            return parent::error($validator->errors()->first());
        }
        
        $client = Client::create($fields);
        
        return parent::success($client->fresh()->toArray(), 201);
    }
    
    
    public function update(Request $request, $uid): JsonResponse
    {
        $fields = $request->only(array_keys(self::$rules));
        $validator = Validator::make($fields, self::$rules);
        
        if ($validator->fails()) {
            return parent::error($validator->errors()->first());
        }
        
        $client = Client::where('uid', $uid)
            ->limit(1)
            ->first();
        
        if (is_null($client)) {
            return parent::error('Client not found.');
        }
        
        $client->update($fields);
        
        return parent::success($client->fresh()->toArray());
    }
    
    
    public function destroy($uid): JsonResponse
    {
        $client = Client::where('uid', $uid)
            ->limit(1)
            ->first();
        
        if (is_null($client)) {
            return parent::error('Client not found.');
        }
        
        CustomClearance::where('customer_uid', $uid)
            ->update(['customer_uid' => null]);
        
        User::where('client_uid', $uid)
            ->update(['client_uid' => null]);
        
        try {
            $client->delete();
        } catch (Exception $exception) {
            return parent::error($exception->getMessage());
        }
        
        return parent::success([], 204);
    }
}
