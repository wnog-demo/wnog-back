<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Document;
use Exception;
use Illuminate\Http\{JsonResponse, Request};
use Validator;

class DocumentController extends Controller
{
    protected static $rules = [
        'name'             => 'required|string|max:255',
        'en_name'          => 'nullable|string|max:255',
        'category_name'    => 'required|string|max:255',
        'en_category_name' => 'nullable|string|max:255',
        'date'             => 'nullable|date_format:"Y-m-d H:i:s"',
    ];
    
    
    public function store(Request $request): JsonResponse
    {
        $rules = array_merge([
            'uid'                  => 'required|string|max:255',
            'custom_clearance_uid' => 'required|string|max:255|exists:custom_clearances,uid',
        ], self::$rules);
        $fields = $request->only(array_keys($rules));
        $validator = Validator::make($fields, $rules);
        
        if ($validator->fails()) {
            return parent::error($validator->errors()
                ->first());
        }
        
        $document = $request->get('document');
        
        if (! preg_match('%^[a-zA-Z0-9/+]*={0,2}$%', substr($document, strpos($document, ',') + 1))) {
            return parent::error('document is not base64');
        }
        
        $fields = array_merge([
            'path' => Document::upload($document),
        ], $fields);
        
        $document = Document::updateOrCreate([
            'uid' => $fields['uid'],
        ], $fields);
        
        return parent::success($document->fresh()
            ->toArray(), 201);
    }
    
    
    public function destroy($uid): JsonResponse
    {
        $status = Document::where('uid', $uid)
            ->first();
        
        try {
            if (is_null($status)) {
                throw new Exception('Document not found.');
            }
            
            $status->delete();
        } catch (Exception $exception) {
            return parent::error($exception->getMessage());
        }
        
        return parent::success([], 204);
    }
}
