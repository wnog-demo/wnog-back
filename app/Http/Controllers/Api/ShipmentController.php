<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\{JsonResponse, Request};
use Validator;

class ShipmentController extends Controller
{
    protected static $rules = [
        'custom_clearance_uid' => 'required|string|max:255|exists:custom_clearances,uid',
        'shipment_type_uid'    => 'exists:shipment_types,uid',
        'quantity'             => 'nullable|string|max:255',
        'weight'               => 'nullable|string|max:255',
    ];
    
    
    public function store(Request $request): JsonResponse
    {
        //        $rules = array_merge(['custom_clearance_uid' => 'required|string|max:255|exists:custom_clearances,uid'], self::$rules);
        $fields = $request->only(array_keys(self::$rules));
        $validator = Validator::make($fields, self::$rules);
        
        if ($validator->fails()) {
            return parent::error($validator->errors()
                ->first());
        }
        
        DB::table('custom_clearance_shipment_type')
            ->updateOrInsert([
                'custom_clearance_uid' => $fields['custom_clearance_uid'],
                'shipment_type_uid'    => $fields['shipment_type_uid'],
            ], $fields);
        
        return parent::success($fields, 201);
    }
    
    
    public function update(Request $request, $uid): JsonResponse
    {
        $fields = $request->only(array_keys(self::$rules));
        $fields['custom_clearance_uid'] = $uid;
        $validator = Validator::make($fields, self::$rules);
        
        if ($validator->fails()) {
            return parent::error($validator->errors()
                ->first());
        }
        
        DB::table('custom_clearance_shipment_type')
            ->updateOrInsert([
                'custom_clearance_uid' => $fields['custom_clearance_uid'],
                'shipment_type_uid'    => $fields['shipment_type_uid'],
            ], $fields);
        
        return parent::success($fields);
    }
}
