<?php

namespace App\Http\Controllers\Api;

use Exception;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\{Request, JsonResponse};
use App\Models\{CustomClearance, Shipper};

class ShipperController extends Controller
{
    protected static $rules = [
        'name'    => 'required|string|max:255',
        'en_name' => 'nullable|string|max:255',
        'address' => 'nullable|string|max:255',
    ];
    
    
    public function store(Request $request): JsonResponse
    {
        $rules = array_merge(['uid' => 'required|string|unique:shippers'], self::$rules);
        $fields = $request->only(array_keys($rules));
        $validator = Validator::make($fields, $rules);
        
        if ($validator->fails()) {
            return parent::error($validator->errors()->first());
        }
        
        $shipper = Shipper::create($fields);
        
        return parent::success($shipper->fresh()->toArray(), 201);
    }
    
    
    public function update(Request $request, $uid): JsonResponse
    {
        $fields = $request->only(array_keys(self::$rules));
        $validator = Validator::make($fields, self::$rules);
        
        if ($validator->fails()) {
            return parent::error($validator->errors()->first());
        }
        
        $shipper = Shipper::where('uid', $uid)
            ->limit(1)
            ->first();
        
        if (is_null($shipper)) {
            return parent::error('Shipper not found.');
        }
        
        $shipper->update($fields);
        
        return parent::success($shipper->fresh()->toArray());
    }
    
    
    public function destroy($uid): JsonResponse
    {
        $shipper = Shipper::where('uid', $uid)
            ->limit(1)
            ->first();
        
        if (is_null($shipper)) {
            return parent::error('Shipper not found.');
        }
        
        CustomClearance::where('shipper_uid', $uid)
            ->update(['shipper_uid' => null]);
        CustomClearance::where('consignee_uid', $uid)
            ->update(['consignee_uid' => null]);
        
        try {
            $shipper->delete();
        } catch (Exception $exception) {
            return parent::error($exception->getMessage());
        }
        
        return parent::success([], 204);
    }
}
