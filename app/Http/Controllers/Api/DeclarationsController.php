<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Declaration;
use Exception;
use Illuminate\Http\{JsonResponse, Request};
use Validator;

class DeclarationsController extends Controller
{
    protected static $rules = [
        'number' => 'nullable|string|max:255',
        'date'   => 'nullable|date_format:"Y-m-d H:i:s"',
    ];
    
    
    public function store(Request $request): JsonResponse
    {
        $rules = array_merge([
            'uid'                  => 'required|string|max:255',
            'custom_clearance_uid' => 'required|string|max:255|exists:custom_clearances,uid',
        ], self::$rules);
        $fields = $request->only(array_keys($rules));
        $validator = Validator::make($fields, $rules);
        
        if ($validator->fails()) {
            return parent::error($validator->errors()
                ->first());
        }
        
        $declaration = Declaration::updateOrCreate([
            'uid' => $fields['uid'],
        ], $fields);
        
        return parent::success($declaration->fresh()
            ->toArray(), 201);
    }
    
    
    public function update(string $uid, Request $request): JsonResponse
    {
        $fields = $request->only(array_keys(self::$rules));
        $validator = Validator::make($fields, self::$rules);
        
        if ($validator->fails()) {
            return parent::error($validator->errors()
                ->first());
        }
        
        $declaration = Declaration::where('uid', $uid)
            ->limit(1)
            ->first();
        
        if (is_null($declaration)) {
            return parent::error('Declaration not found.');
        }
        
        $declaration->update($fields);
        
        return parent::success($declaration->fresh()
            ->toArray(), 201);
    }
    
    
    public function destroy($uid): JsonResponse
    {
        $status = Declaration::where('uid', $uid)
            ->first();
        
        try {
            if (is_null($status)) {
                throw new Exception('Declaration not found.');
            }
            
            $status->delete();
        } catch (Exception $exception) {
            return parent::error($exception->getMessage());
        }
        
        return parent::success([], 204);
    }
}
