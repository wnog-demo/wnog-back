<?php

namespace App\Http\Controllers\Api\Status;

use App\Http\Controllers\Controller;
use App\Models\Stage\Coming;
use Exception;
use Illuminate\Http\{JsonResponse, Request};
use Validator;

class ComingController extends Controller
{
    protected static $rules = [
        'custom_clearance_uid' => 'required|string|max:255|exists:custom_clearances,uid',
        'name'                 => 'required|string|in:eta,ata',
        'comment'              => 'nullable|string|max:255',
        'en_comment'           => 'nullable|string|max:255',
        'date'                 => 'required|date_format:"Y-m-d H:i:s"',
        'date_status'          => 'nullable|date_format:"Y-m-d H:i:s"',
    ];
    
    
    public function store(Request $request): JsonResponse
    {
        $fields = $request->only(array_keys(self::$rules));
        $validator = Validator::make($fields, self::$rules);
        
        if ($validator->fails()) {
            return parent::error($validator->errors()->first());
        }
        
        $status = Coming::updateOrCreate([
            'custom_clearance_uid' => $fields['custom_clearance_uid'],
            'name'                 => $fields['name'],
        ], $fields);
        
        return parent::success($status->fresh()->toArray(), 201);
    }
    
    
    public function destroy($uid, $name): JsonResponse
    {
        $status = Coming::where('name', $name)
            ->where('custom_clearance_uid', $uid)
            ->first();
        
        try {
            if (is_null($status)) {
                throw new Exception('Status "coming" not found.');
            }
            
            $status->delete();
        } catch (Exception $exception) {
            return parent::error($exception->getMessage());
        }
        
        return parent::success([], 204);
    }
}
