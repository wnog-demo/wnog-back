<?php

namespace App\Http\Controllers\Api\Status;

use App\Http\Controllers\Controller;
use App\Models\Stage\Declaration as DeclarationStage;
use Exception;
use Illuminate\Http\{JsonResponse, Request};
use Validator;

class DeclarationController extends Controller
{
    protected static $rules = [
        'declaration_uid' => 'required|string|max:255|exists:declarations,uid',
        'name'            => 'required|string|in:registered,assigned-inspection,inspection-completed,conditional-release,released,refusal-to-issue,pre-declaration',
        'comment'         => 'nullable|string|max:255',
        'en_comment'      => 'nullable|string|max:255',
        'date_status'     => 'nullable|date_format:"Y-m-d H:i:s"',
    ];
    
    
    public function store(Request $request): JsonResponse
    {
        $fields = $request->only(array_keys(self::$rules));
        $validator = Validator::make($fields, self::$rules);
        
        if ($validator->fails()) {
            return parent::error($validator->errors()
                ->first());
        }
        
        $stage = DeclarationStage::updateOrCreate([
            'declaration_uid' => $fields['declaration_uid'],
            'name'            => $fields['name'],
        ], $fields);
        
        return parent::success($stage->fresh()
            ->toArray(), 201);
    }
    
    
    public function destroy($uid, $name): JsonResponse
    {
        $status = DeclarationStage::where('name', $name)
            ->where('declaration_uid', $uid)
            ->first();
        
        try {
            if (is_null($status)) {
                throw new Exception('Status "declaration" not found.');
            }
            
            $status->delete();
        } catch (Exception $exception) {
            return parent::error($exception->getMessage());
        }
        
        return parent::success([], 204);
    }
}
