<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    
    protected static function success(array $data = [], int $status = 200): JsonResponse
    {
        $response = [
            'status' => 'success',
        ];
        
        if (! empty($data)) {
            $response['data'] = $data;
        }
        
        return response()->json($response, $status);
    }
    
    
    protected static function error(string $message = '', int $status = 400): JsonResponse
    {
        $response = [
            'status' => 'error',
        ];
        
        if (! empty($message)) {
            $response['message'] = $message;
        }
        
        return response()->json($response, $status);
    }
}
