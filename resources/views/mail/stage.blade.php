@extends('mail.layouts.template')

@section('content')

{!! $data['html'] !!}

<table style="margin: 0; padding: 0; border-spacing: 0; border: none;">
    <tr style="margin: 0; padding: 0;">
        <td height="40" style="margin: 0; padding: 0;"></td>
    </tr>
</table>
<div class="action" style="margin: 0; padding: 0;">
    <div class="button" style="margin: 0; padding: 0;">
        <table style="margin: 0; width: 236px; padding: 0; border-spacing: 0; border: none; background-color: #2CABFC;">
            <td width="50" style="margin: 0; padding: 0;"></td>
            <td align="center" valign="middle" height="54">
                <a href="{{ $data['url'] }}"
                   style="margin: 0; display: block; height:54px; line-height: 54px; padding: 0; text-decoration: none; color: #2CABFC; font-size: 11px; font-weight: bold; letter-spacing: 1px;">
                    <font face="sans-serif" color="#ffffff" style="margin: 0; padding: 0;">ОТКРЫТЬ ЗАЯВКУ</font>
                </a>
            </td>
            <td width="50" style="margin: 0; padding: 0;"></td>
        </table>
    </div>
</div>
<table class="head" style="margin: 0;  width: 100%; max-width: 550px; padding: 0; border-spacing: 0; border: none;">
    <tr style="margin: 0; padding: 0;">
        <td colspan="2" height="80" style="margin: 0; padding: 0;"></td>
    </tr>
    <tr style="margin: 0; padding: 0;">
        <td style="margin: 0; padding: 0; border: 1px">
            <table class="head" style="margin: 0; padding: 0; width: 100%; border-spacing: 0; border: none;">
                <tr>
                    <table cellspacing="0" cellpadding="0" width="600px" border="0">
                        <tr>
                            <td align="left" valign="top" width="600px" height="1"
                                style="background-color: #f0f0f0; border-collapse:collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; mso-line-height-rule: exactly; line-height: 1px;">
                                <!--[if gte mso 15]>&nbsp;<![endif]--></td>
                        </tr>
                    </table>
                </tr>
                <tr style="margin: 0; padding: 0;">
                    <td colspan="2" height="30" style="margin: 0; padding: 0;"></td>
                </tr>
                <tr style="margin: 0; padding: 0;">
                    <td style="margin: 0; padding: 0;">
                        <table class="head"
                               style="margin: 0; padding: 0; border-spacing: 0; border: none; width: 100%">
                            <tr style="margin: 0; padding: 0;">
                                <td style="margin: 0; padding: 0;">
                                    <a href="{{ config('app.url-front') }}" target="_blank">
                                        <img src="{{ config('app.url') . '/upload/logo.png' }}" alt="eWnog"
                                             style="margin: 0; padding: 0; width: 80px; height: 21px;">
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr style="margin: 0; padding: 0;">
                    <td colspan="2" height="15" width="600px" style="margin: 0; padding: 0;"></td>
                </tr>
                <tr style="margin: 0; padding: 0;">

                    <table width="600" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="600" colspan="2"></td>
                        </tr>
                        <tr>
                            <td width="460" style="margin: 0; padding: 0;">
                                <p style="margin: 0; padding: 0; font-size: 12px; font-weight: 400; line-height: 16px;">
                                    <font face="sans-serif"
                                          style="margin: 0; padding: 0; color: rgba(37, 40, 65, 0.4);">2019 © eWNOG
                                        Logistic</font></p>
                            </td>
                            <td width="140" style="margin: 0; padding: 0;">
                                <a href="{{ config('app.url-front') }}/settings"
                                   style="margin: 0; padding: 0; line-height: 16px; text-decoration: none; font-size: 12px; font-weight: 400;">
                                    <font face="sans-serif" color="#2CABFC" style="margin: 0; padding: 0;">Отписаться
                                        от рассылки</font></a>
                            </td>
                        </tr>
                    </table>
                </tr>
                <tr style="margin: 0; padding: 0;">
                    <td colspan="2" height="40" style="margin: 0; padding: 0;"></td>
                </tr>
            </table>
        </td>
    </tr>
</table>

@endsection
