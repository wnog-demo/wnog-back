<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" style="margin: 0; padding: 0;">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--<![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--[if (gte mso 9)|(IE)]>
    <style type="text/css">
        table {
            border-collapse: collapse;
        }
    </style>
    <![endif]-->
</head>
<body style="margin: 0; padding: 0; color: #252841;">
<table style="margin: 0; border-spacing: 0; border: none; padding: 0; width: 100%; font-family: sans-serif; line-height: 1; -webkit-font-smoothing: antialiased;">
    <tr style="margin: 0; padding: 0;">
        <td style="margin: 0; padding: 0; background-color: #eef3f5;">
            <table class="content"
                   style="margin: 0; padding: 0; border-spacing: 0; border: none; width: 100%; background-color: #fff; line-height: 1; font-weight: 300; color: #467586;">
                <tr style="margin: 0; padding: 0;">
                    <td colspan="3" height="60" style="margin: 0; padding: 0; background-color: #fff;"></td>
                </tr>
                <tr style="margin: 0; padding: 0;">
                    <td width="40" style="margin: 0; padding: 0; background-color: #fff;"></td>
                    <td style="margin: 0; padding: 0; background-color: #fff;">

                        @yield('content')

                    </td>
                    <td width="15" style="margin: 0; padding: 0; background-color: #fff;"></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
