<?php

return [
    'awb-place-of-receipt'  => 'Место отправки',
    'awb-port-of-loading'   => 'Аэропорт погрузки',
    'awb-port-of-discharge' => 'Аэропорт выгрузки',
    'awb-place-of-delivery' => 'Место доставки',
    
    'bol-place-of-receipt'  => 'Место отправки',
    'bol-port-of-loading'   => 'Порт погрузки',
    'bol-port-of-discharge' => 'Порт выгрузки',
    'bol-place-of-delivery' => 'Место доставки',
    
    'rwb-place-of-receipt'  => 'Место отправки',
    'rwb-port-of-loading'   => 'Станция погрузки',
    'rwb-port-of-discharge' => 'Станция выгрузки',
    'rwb-place-of-delivery' => 'Место доставки',
    
    'cmr-place-of-receipt'  => 'Место отправки',
    'cmr-port-of-loading'   => 'Место погрузки',
    'cmr-port-of-discharge' => 'Место выгрузки',
    'cmr-place-of-delivery' => 'Место доставки',
];
