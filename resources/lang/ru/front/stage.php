<?php

return [
    'title'          => 'Статусы таможенного оформления',
    'greenlight'     => 'Green Light',
    'coming'         => 'Прибытие',
    'declaration'    => 'Декларация',
    'transportation' => 'Транспортировка',
];
