<?php

return [
    'email'            => 'Email',
    'password'         => 'Пароль',
    'client-ref'       => 'Введите клиент-референс',
    'forgot'           => 'Забыли пароль?',
    'past-password'    => 'Прошлый пароль',
    'new-password'     => 'Новый пароль',
    'confirm-password' => 'Повторите пароль',
];
