<?php

return [
    'required'     => 'Обязательное поле',
    'email'        => 'Неверный формат email',
    'password-min' => 'Минимальное количество символов - :count',
];
