<?php

return [
    'created'             => 'Создана :date',
    'notify-by-mail'      => 'Уведомлять на почту',
    'change-password'     => 'Сменить пароль',
    'your-data'           => 'Ваши данные',
    'your-login'          => ':login — ваш логин',
    'your-email'          => ':email — на ваш e-mail',
    'client-ref'          => 'Клиент-референс',
    'status-date'         => 'Статус и дата',
    'customs-coordinator' => 'Таможня и координатор',
    'transportation'      => 'Транспортировка',
    'direction'           => 'Направление',
    'greenlight'          => 'Green Light',
    'arrival'             => 'Прибытие',
    'declaration'         => 'Декларация',
    'customs'             => 'Таможня',
    'archive'             => 'Архив',
    'import'              => 'Импорт',
    'route'               => 'Маршрут',
    'we-will-send-letter' => 'Мы отправим письмо со ссылкой для восстановлением пароля на e-mail, указанный при регистрации в системе',
];
