<?php

return [
    'awb-place-of-receipt'  => 'Place of departure',
    'awb-port-of-loading'   => 'Airport of loading',
    'awb-port-of-discharge' => 'Airport of discharge',
    'awb-place-of-delivery' => 'Delivery location',
    
    'bol-place-of-receipt'  => 'Place of departure',
    'bol-port-of-loading'   => 'Port of loading',
    'bol-port-of-discharge' => 'Port of discharge',
    'bol-place-of-delivery' => 'Delivery location',
    
    'rwb-place-of-receipt'  => 'Place of departure',
    'rwb-port-of-loading'   => 'Station of loading',
    'rwb-port-of-discharge' => 'Station of discharge',
    'rwb-place-of-delivery' => 'Delivery location',
    
    'cmr-place-of-receipt'  => 'Place of departure',
    'cmr-port-of-loading'   => 'Place of loading',
    'cmr-port-of-discharge' => 'Place of discharge',
    'cmr-place-of-delivery' => 'Delivery location',
];
