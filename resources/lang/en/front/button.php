<?php

return [
    'sign-in'      => 'Enter',
    'sign-up'      => 'Register',
    'sign-out'     => 'Logout',
    'to-home'      => 'On main page',
    'save'         => 'Save',
    'next'         => 'Continue',
    'reset-filter' => 'Clear filters',
    'all-request'  => 'All custom clearances',
    'send'         => 'Send',
];
