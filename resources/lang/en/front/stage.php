<?php

return [
    'title'          => 'Customs clearance statuses',
    'greenlight'     => 'Green Light',
    'coming'         => 'Arrival',
    'declaration'    => 'Declaration',
    'transportation' => 'Transportation',
];
