<?php

return [
    'email'            => 'Email',
    'password'         => 'Password',
    'client-ref'       => 'Enter Client-reference',
    'forgot'           => 'Forgot your password?',
    'past-password'    => 'Current password',
    'new-password'     => 'New password',
    'confirm-password' => 'Confirm password',
];
