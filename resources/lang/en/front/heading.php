<?php

return [
    'setting'           => 'Settings',
    'no-request'        => 'No custom clearances by setted parameters',
    'page-not-found'    => 'Error! This page does not exists.',
    'enter-the-cabinet' => 'Log into your account to track customs clearance',
    'create-password'   => 'Create a password to start tracking your customs clearance',
    'email-sent'        => 'Email sent',
    'restore-access'    => 'Restore access',
];
