<?php

return [
    'required'     => 'Obligatory field',
    'email'        => 'Invalid email format',
    'password-min' => 'Minimum number of characters - :count',
];
