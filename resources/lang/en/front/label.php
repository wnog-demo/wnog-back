<?php

return [
    'created'             => 'Created :date',
    'notify-by-mail'      => 'Your data',
    'change-password'     => 'E-mail notifications',
    'your-data'           => 'change password',
    'your-login'          => ':login — your login',
    'your-email'          => ':email — on your e-mail',
    'client-ref'          => 'Client-reference',
    'status-date'         => 'Status and date',
    'customs-coordinator' => 'Customs and coordinator',
    'transportation'      => 'Transportation',
    'direction'           => 'Direction',
    'greenlight'          => 'Green Light',
    'arrival'             => 'Arrival',
    'declaration'         => 'Declaration',
    'customs'             => 'Customs',
    'transportation'      => 'Transportation',
    'archive'             => 'Archive',
    'import'              => 'Import',
    'route'               => 'Route',
    'we-will-send-letter' => 'We will send an email with a link to reset your password to the e-mail you provided during registration',
];
