<?php

use App\GraphQL\Mutation;
use App\GraphQL\Query;
use App\GraphQL\Type;
use Rebing\GraphQL\{Error\ValidationError, GraphQL, GraphQLController, Support\PaginationType};

return [
    'types' => [
        'Response'         => Type\ResponseType::class,
        'User'             => Type\UserType::class,
        'UserRole'         => Type\UserRoleType::class,
        'Client'           => Type\ClientType::class,
        'Notification'     => Type\NotificationType::class,
        'CustomClearance'  => Type\CustomClearanceType::class,
        'Stages'           => Type\StagesType::class,
        'Stage'            => Type\StageType::class,
        'Transportation'   => Type\TransportationType::class,
        'Place'            => Type\PlaceType::class,
        'Custom'           => Type\CustomType::class,
        'Shipper'          => Type\ShipperType::class,
        'Shipment'         => Type\ShipmentType::class,
        'Product'          => Type\ProductType::class,
        'Route'            => Type\RouteType::class,
        'Contact'          => Type\ContactType::class,
        'Vessel'           => Type\VesselType::class,
        'DocumentCategory' => Type\DocumentCategoryType::class,
        'Document'         => Type\DocumentType::class,
    ],
    
    'schemas' => [
        'default' => [
            
            'query' => [
                'user'            => Query\UserQuery::class,
                'notification'    => Query\NotificationQuery::class,
                'customClearance' => Query\CustomClearanceQuery::class,
                'filter'          => Query\FilterQuery::class,
                'lang'            => Query\LangQuery::class,
            ],
            
            'mutation' => [
                'setPassword'         => Mutation\User\SetPasswordMutation::class,
                'checkSetPassword'    => Mutation\User\CheckSetPasswordMutation::class,
                'getToken'            => Mutation\User\GetTokenMutation::class,
                'isAuth'              => Mutation\User\IsAuthMutation::class,
                'changePassword'      => Mutation\User\ChangePasswordMutation::class,
                'restorePassword'     => Mutation\User\RestorePasswordMutation::class,
                'updateNotifications' => Mutation\UpdateNotificationsMutation::class,
                'downloadDocuments'   => Mutation\DownloadDocumentsMutation::class,
            ],
            
            'middleware' => ['graphql'],
            'method'     => ['post'],
        ],
    ],
    
    'prefix' => 'graphql',
    
    'routes' => '{graphql_schema?}',
    
    'controllers' => GraphQLController::class . '@query',
    
    'middleware' => [],
    
    'route_group_attributes' => [],
    
    'default_schema' => 'default',
    
    'error_formatter' => [GraphQL::class, 'formatError'],
    
    'errors_handler' => function (array $errors) {
        $error = current($errors);
        
        $results = ['message' => $error->getMessage()];
        
        if (($previous = $error->getPrevious()) && $previous instanceof ValidationError) {
            $results['validation'] = $previous->getValidatorMessages();
        }
        
        return $results;
    },
    
    'params_key' => 'variables',
    
    'security' => [
        'query_max_complexity'  => null,
        'query_max_depth'       => null,
        'disable_introspection' => false,
    ],
    
    'pagination_type' => PaginationType::class,
    
    'graphiql' => [
        'display' => false,
    ],
];
