<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class ContactTest extends TestCase
{
    public function testStore(): void
    {
        $corrects = [
            [
                'uid'     => '9db9c9cc-f5b9-11e7-90d1-60a44c5f409e',
                'name'    => 'Игорь',
                'en_name' => 'Igor',
                'phones'  => ['+7 999 000-00-10'],
                'email'   => 'igor@gmail.com',
                'gender'  => 'male',
                'role'    => 'Управляющий',
            ],
            [
                'uid'     => '9db9c9cc-f5b9-11e7-90d1-60a44c5f409q',
                'name'    => 'Светлана',
                'en_name' => 'Svetlana',
                'phones'  => ['+7 999 000-00-20', '+7 999 000-00-30'],
                'email'   => 'sveta@gmail.com',
                'gender'  => 'female',
                'role'    => 'Бухгалтер',
            ],
            [
                'uid'  => '9db9c9cc-f5b9-11e7-90d1-60a44c5f409p',
                'name' => 'Андрей',
            ],
        ];
        
        $wrongs = [
            'The uid field is required.'      => [
                'name'    => 'Игорь',
                'en_name' => 'Igor',
                'phones'  => ['+7 999 000-00-10'],
                'email'   => 'igor@gmail.com',
                'gender'  => 'male',
                'role'    => 'Управляющий',
            ],
            'The name field is required.'     => [
                'uid'     => '9db9c9cc-f5b9-11e7-90d1-60a44c5f409z',
                'en_name' => 'Igor',
                'phones'  => ['+7 999 000-00-10'],
                'email'   => 'igor@gmail.com',
                'gender'  => 'male',
                'role'    => 'Управляющий',
            ],
            'The uid has already been taken.' => [
                'uid'     => '9db9c9cc-f5b9-11e7-90d1-60a44c5f409e',
                'name'    => 'Игорь',
                'en_name' => 'Igor',
                'phones'  => ['+7 999 000-00-10'],
                'email'   => 'igor@gmail.com',
                'gender'  => 'male',
                'role'    => 'Управляющий',
            ],
            'The phones must be an array.'    => [
                'uid'     => '9db9c9cc-f5b9-11e7-90d1-60a44c5f409x',
                'name'    => 'Игорь',
                'en_name' => 'Igor',
                'phones'  => '+7 999 000-00-10',
                'email'   => 'igor@gmail.com',
                'gender'  => 'male',
                'role'    => 'Управляющий',
            ],
            'The selected gender is invalid.' => [
                'uid'     => '9db9c9cc-f5b9-11e7-90d1-60a44c5f409c',
                'name'    => 'Игорь',
                'en_name' => 'Igor',
                'phones'  => ['+7 999 000-00-10'],
                'email'   => 'igor@gmail.com',
                'gender'  => 'test',
                'role'    => 'Управляющий',
            ],
        ];
        
        foreach ($corrects as $data) {
            $this->json('POST', '/api/contacts', $data)
                ->assertStatus(201)
                ->assertJsonStructure([
                    'status',
                    'data' => [
                        'uid',
                        'name',
                        'en_name',
                        'phones',
                        'email',
                        'gender',
                        'role',
                    ],
                ]);
        }
        
        foreach ($wrongs as $message => $data) {
            $this->json('POST', '/api/contacts', $data)
                ->assertStatus(400)
                ->assertJson([
                    'status'  => 'error',
                    'message' => $message,
                ]);
        }
    }


//    public function testUpdate(): void
//    {
//    }
//
//
//    public function testDestroy(): void
//    {
//    }
}
