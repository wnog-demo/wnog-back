<?php

use App\Models\CustomClearanceLog;
use App\Models\User;

Route::get('downloads/{filename}', function ($filename) {
    if (! Storage::exists('public/downloads/' . $filename)) {
        return response()->json([
            'status'  => 'error',
            'message' => 'Method not found.',
        ], 404);
    }
    
    return response()
        ->download(Storage::path('public/downloads/' . $filename))
        ->deleteFileAfterSend();
});

Route::get('/actions', function (Illuminate\Http\Request $request) {
    if ($request->get('token') != '$2y$10$Z3wnHw4dNzc.W6eeGWnM0uS85nSQwMgSBq4RMbZAZh6w88TZNjWCm') {
        return response()->json([
            'status'  => 'error',
            'message' => 'Method not found.',
        ], 404);
    }
    
    $html = '';
    
    $logs = CustomClearanceLog::all()
        ->groupBy(['date', 'user_uid'])
        ->reverse();
    
    foreach ($logs as $date => $users) {
        $html .= sprintf('<h3>%s</h3>', $date);
        
        foreach ($users as $userUid => $items) {
            if ($first = $items->first()) {
                $user = $first->user;
                $html .= sprintf('<p>%s &mdash; %d %s<p>', $user->email, $items->count(), Lang::choice('действие|действия|действий', $items->count(), [], 'ru'));
            }
        }
    }
    
    return $html;
});

if (App::environment() != 'production') {
    Route::get('users', function () {
        return User::where('is_active', true)
            ->get(['role_id', 'email', 'name']);
    });
    
    Route::get('api-logs-clear', function () {
        $storage = Storage::disk('public');
        
        if ($storage->exists('logs/api')) {
            $storage->deleteDirectory('logs/api');
        }
        
        return redirect('/api-logs');
    });
    
    Route::get('api-logs', function () {
        $print = function (string $path) {
            $storage = Storage::disk('public');
            
            if (! $storage->exists($path)) {
                return '<i>логов нет</i>';
            }
            
            if ($files = scandir($path)) {
                $files = array_values(array_diff($files, ['.', '..']));
                $html = '<a href="/api-logs-clear">Очистить логи</a><br><ul>';
                
                foreach ($files as $fileName) {
                    $html .= sprintf('<li class="file"><a href="%s" target="_blank">%s</a>', asset(sprintf('%s/%s', $path, $fileName)), htmlspecialchars($fileName));
                }
                
                $html .= '</ul>';
                
                return $html;
            }
        };
        
        return $print('logs/api');
    });
}
