<?php

use App\Http\Controllers\Api;

$apiOnly = ['store', 'update', 'destroy'];

// Пользователи
Route::resource('user', Api\UserController::class)
     ->only($apiOnly);

// Обновление адреса эл. почты
Route::put('user/{user}/update-email/', [Api\UserController::class, 'updateEmail'])
     ->name('user.update-email');

// Контактные лица вног
Route::resource('contacts', Api\ContactController::class)
     ->only($apiOnly);

// Корабли
Route::resource('vessels', Api\VesselController::class)
     ->only($apiOnly);

// Отправители-получатели
Route::resource('shippers', Api\ShipperController::class)
     ->only($apiOnly);

// Клиенты
Route::resource('clients', Api\ClientController::class)
     ->only($apiOnly);

// Таможни
Route::resource('customs', Api\CustomController::class)
     ->only($apiOnly);

// Продуктовая линейка
Route::resource('products', Api\ProductController::class)
     ->only($apiOnly);

// Места отправки-доставки
Route::resource('places', Api\PlaceController::class)
     ->only($apiOnly);

// Наименование грузов
Route::resource('shipment-types', Api\ShipmentTypeController::class)
     ->only($apiOnly);

// Таможенное оформление
Route::resource('customs-clearances', Api\CustomClearanceController::class)
     ->only($apiOnly);

// Грузы для таможенное оформление
Route::resource('shipments', Api\ShipmentController::class)
     ->only(['store', 'update']);

// Декларации для ТО
Route::resource('declarations', Api\DeclarationsController::class)
     ->only($apiOnly);

// Документы для ТО
Route::resource('documents', Api\DocumentController::class)
     ->only($apiOnly);

// Статус таможенного оформления (green light)
Route::post('status/greenlight', [Api\Status\GreenLightController::class, 'store'])
     ->name('status.greenlight.store');
Route::delete('status/greenlight/{custom_clearance_uid}/{status_name}', [Api\Status\GreenLightController::class, 'destroy'])
     ->name('status.greenlight.destroy');

// Статус таможенного оформления (прибытия груза)
Route::post('status/coming', [Api\Status\ComingController::class, 'store'])
     ->name('status.coming.store');
Route::delete('status/coming/{custom_clearance_uid}/{status_name}', [Api\Status\ComingController::class, 'destroy'])
     ->name('status.coming.destroy');

// Статус таможенного оформления (декларации)
Route::post('status/declaration', [Api\Status\DeclarationController::class, 'store'])
     ->name('status.declaration.store');
Route::delete('status/declaration/{declaration_uid}/{status_name}', [Api\Status\DeclarationController::class, 'destroy'])
     ->name('status.declaration.destroy');

// Статус таможенного оформления (перевозки)
Route::post('status/transportation', [Api\Status\TransportationController::class, 'store'])
     ->name('status.transportation.store');
Route::delete('status/transportation/{custom_clearance_uid}/{status_name}', [Api\Status\TransportationController::class, 'destroy'])
     ->name('status.transportation.destroy');
